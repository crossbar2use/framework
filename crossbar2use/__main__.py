#!/usr/bin/env python3

import os,sys,click

from urllib.parse import urlparse

import json,yaml

from decouple import config

bpath = lambda *x: os.path.join(os.path.dirname(__file__), *x)
rpath = lambda *x: os.path.join(os.path.abspath(os.getcwd()), *x)

#*******************************************************************************

def shell(cmd, *args):
    stmt = [cmd]

    for x in args:
        x = str(x)

        if " " in x:
            x = '"'+x+'"'

        stmt.append(x)

    os.system(' '.join(stmt))

#*******************************************************************************

def save_json(y,*x):
    with open(rpath(*x),'w+') as f:
        f.write(json.dumps(y))

load_json = lambda *x: json.loads(open(rpath(*x)).read())

#*******************************************************************************

def save_yaml(y,*x):
    with open(rpath(*x),'a') as f:
        f.write(yaml.dumps(y))

load_yaml = lambda *x: yaml.load(open(rpath(*x)), Loader=yaml.FullLoader)

################################################################################

@click.group()
#@click.option('--count', default=1, help='Number of greetings.')
@click.option('--debug/--no-debug', default=False)
@click.pass_context
def cli(ctx,debug):
    ctx.obj['guid'] = config('APP_KEY',default="default")

    pth = rpath('manifest.yaml')

    if os.path.exists(pth):
        ctx.obj.update(load_yaml(pth))

    click.echo('Debug mode is %s' % ('on' if debug else 'off'))

    os.environ['PYTHONPATH'] = ':'.join([
        rpath('graph','que'),
    ])

#*******************************************************************************

def cross_static(folder):
    return {
        "type": "static",
        "directory": "fodler",
    }

def cross_applet(folder):
    pass

def cross_server(port,host='localhost',link=None):
    resp = {
        'host': host,
        'type': 'reverseproxy',
        'port': int(port),
    }
    
    if link is not None:
        resp['path'] = link
    
    return resp

def cross_worker(command,*argument,**kwargs):
    environ = kwargs.get('environ',{})

    if 'port' in kwargs:
        environ['PORT'] = str(kwargs['port'])

    folder = kwargs.get('folder','.')

    return {
          "executable" : command,
          "arguments" : argument,
          "options" : {
             "env" : {
                "vars" : environ,
                "inherit" : True
             },
             "workdir" : folder
           },
           "type" : "guest"
        }

#*******************************************************************************

@cli.command('cross')
@click.option('--port', default=config('PORT',cast=int,default=4747), help='Port to listen to.')
@click.option('--mode', type=click.Choice([
    'host','node','talk','wsgi','file',
], case_sensitive=False), default=config('APP_MODE',default="talk"), help='Crossbar Mode to use.')
#@click.option('--port', default=config('PORT',cast=int,default=4747), help='Port to listen to.')
@click.option('--is-all/--no-all', default=False, help='If all flags must be activated.')
@click.option('--is-spy/--no-spy', default=False, help='If a Scrapy daemon is to be embedded.')
@click.option('--is-tor/--no-tor', default=False, help='If a Tor service is to be published.')
@click.option('--is-dns/--no-dns', default=False, help='If a DNS authority is to be served.')
@click.option('--is-web/--no-web', default=False, help='If a Web proxy is to be transparent.')
@click.pass_context
def cross_foo_bar(ctx,port,mode,is_all,is_spy,is_tor,is_dns,is_web):
    """Run CrossBar for custom Specs."""

    pth = load_json('nerve',"route.json")
    job = load_json('nerve',"works.json")
    acl = load_json('nerve',"topic.json")

    maps = {}

    for key in ['model','daten','graph','nerve']:
        maps[key] = {}

    has_it = []

    #***************************************************************************
    
    if mode in ('node','host'):
        has_it += ['local','crawl','hubot']
    elif mode=='wsgi':
        pth['/'] = {
            "type" : "wsgi", 
            "package" : "local",
            "application" : "app"
        }
    elif mode=='talk':
        pth['/'] = cross_server(port=7001)

        has_it += ['hubot','model']
    else:
        if is_spy:
            pth['/'] = cross_server(port=6800)
        else:
            pth['/'] = {
                "type" : "static", 
                "directory" : "pages/"
            }

    if is_spy:
        has_it += ['crawl']

    has_it += ['proxy']

    #***************************************************************************
    
    if mode not in ['hubot']:
        has_it += ['model','daten','graph','nerve']

    has_it += ['vocab','facts']
    #has_it += ['knows']
    has_it += ['memex','think']
    
    #***************************************************************************

    if ('local' in has_it) or is_all:
        if mode=='host':
            pth['/'] = cross_server(port=80)

            job.append(cross_worker("node","local.js",'on',port=80))
        else:
            pth['/'] = cross_server(port=8080)

            job.append(cross_worker("node","local.js",'off',port=8080))

        #pth['landing'] = cross_server(port=8080)

    #***************************************************************************
    
    if ('crawl' in has_it) or is_all:
        pth['crawl'] = cross_server(port=6800)

        job.append(cross_worker("python","-m","crossbar2use","crawl","--port","6800",port=6800))

    if ('hubot' in has_it) or is_all:
        pth['hubot'] = cross_server(port=7000)

        job.append(cross_worker("python","-m","crossbar2use","hubot","--port","7000",port=7000))
    
    if ('parse' in has_it) or is_all:
        maps['model']['api'] = dict(port=7001,link='/model/api')
        maps['graph']['api'] = dict(port=7001,link='/graph/api')
        maps['daten']['api'] = dict(port=7001,link='/daten/api')

        job.append(cross_worker("python","-m","crossbar2use","nosql",port=7001,folder='model/api'))

    #***************************************************************************
    
    if ('model' in has_it) or is_all:
        pass

    if ('daten' in has_it) or is_all:
        maps['graph']['tab'] = dict(port=7002,link='/graph/tab')
        maps['daten']['tab'] = dict(port=7002,link='/daten/tab')
        maps['nerve']['tab'] = dict(port=7002,link='/nerve/tab')

        job.append(cross_worker("python","-m","crossbar2use","daten",port=7002))

    #***************************************************************************
    
    if ('graph' in has_it) or is_all:
        maps['graph']['ql']  = dict(port=7003,link='/graph/ql')
        maps['graph']['iql'] = dict(port=7003,link='/graph/iql')

        job.append(cross_worker("python","-m","crossbar2use","graph",port=7003))

    if ('nerve' in has_it):
        if 'NEO4J_LINK' in os.environ:
            maps['graph']['bag'] = dict(port=7004,link='/graphql')
            #maps['daten']['bag'] = dict(port=7004,link='/')
            #maps['nerve']['bag'] = dict(port=7004,link='/nerve/bag')

            job.append(cross_worker("python","-m","crossbar2use","graph",port=7004))
        else:
            maps['graph']['bag'] = dict(port=7004,link='/graph/bag')
            maps['daten']['bag'] = dict(port=7004,link='/daten/bag')
            maps['nerve']['bag'] = dict(port=7004,link='/nerve/bag')

            job.append(cross_worker("python","-m","crossbar2use","sqldb",port=7004))

    #***************************************************************************
    
    if ('proxy' in has_it) or is_all:
        maps['graph']['web'] = dict(port=7005,link='/')

        job.append(cross_worker("python","-m","crossbar2use","proxy",port=7005))

    if ('vocab' in has_it) or is_all:
        maps['model']['lod'] = dict(port=7006)

        job.append(cross_worker("python","-m","crossbar2use","vocab",port=7006))

    if ('facts' in has_it) or is_all:
        maps['nerve']['lod'] = dict(port=7007)

        job.append(cross_worker("python","-m","crossbar2use","facts",port=7007))

    if ('knows' in has_it) or is_all:
        maps['daten']['bag'] = dict(port=7008)

        job.append(cross_worker("python","-m","crossbar2use","knows",port=7008))

    if ('think' in has_it) or is_all:
        maps['graph']['que'] = dict(port=7009)

        job.append(cross_worker("python","-m","crossbar2use","think",port=7009))

    #***************************************************************************
    
    for key,lst in maps.items():
        pth[key] = {
            "type" : "path",
            "paths" : {}
        }
        for sub,cfg in lst.items():
            pth[key]['paths'][sub] = cross_server(
                cfg.get('port',7001), cfg.get('host',"localhost")
            )
        pth[key]['paths']['/'] = pth['/']

    #***************************************************************************
    
    if False:
        acl += [{
           "permissions" : [
              {
                 "allow" : {
                    "call" : False,
                    "subscribe" : False,
                    "register" : True,
                    "publish" : False
                 },
                 "match" : "exact",
                 "uri" : "memexdb.authenticate",
                 "cache" : True,
                 "disclose" : {
                    "caller" : False,
                    "publisher" : False,
                 }
              }
           ],
           "name" : "authenticator"
        }]
        
    trs = {
        "websocket": {
            "serializers" : [
                "json",
            ],
        },
        "rawsocket": {
        },
        "web": {
            "paths": pth,
        },
        "endpoint" : { "type" : "tcp", "port" : port },
        "type" : "universal",
    }
        
    pth["nerve"]['paths']["sck"] = {
        "serializers" : [
            "json"
        ],
        #"auth": {
        #    "ticket": {
        #        "type": "dynamic",
        #        "authenticator": "tayaa.authenticate",
        #        "authenticator-realm": "tayaa"
        #    }
        #},
        "type" : "websocket"
    }

    trs = {
        "paths": pth,
        "endpoint" : { "type" : "tcp", "port" : port },
        "type" : "web",
    }

    cfg = {
       "options" : {
          "pythonpath" : [
             "."
          ]
       },
       "components" : [],
       "transports" : [
          trs
       ],
       "realms" : [
          {
             "name" : "memexdb",
             "roles" : acl
          }
       ],
       "type" : "router",
    }
    
    if is_tor:
        trs['endpoint'] = {
            "type": "onion",
            "port": port,
            "tor_control_endpoint": {
                "type": "unix",
                "path": "/run/tor/control.authcookie"
            },
            "private_key_file": "key.tor"
        }
        
        cfg['transports'].append(trs)

    save_json({
       "workers" : [cfg] + job,
       "version" : 2
    },"config.json")

    os.environ['PORT'] = str(port)

    shell("crossbar","start","--cbdir",".")

#*******************************************************************************

@cli.command('hubot')
@click.option('--name', default=config('BOT_ALIAS',default=None), help='Alias name to address.')
@click.option('--port', default=config('EXPRESS_PORT',cast=int,default=4747), help='Port to listen to.')
#@click.option('--kind', choice=['telegram','slack'], default="telegram", help='Adapter to use.')
@click.option('--kind', type=click.Choice([
    'irc','xmpp','slack','telegram',
], case_sensitive=False), default=config('BOT_CLASS',default="irc"), help='Adapter to use.')
@click.option('--plug','-p', multiple=True, default=[])
@click.pass_context
def hubot_talk(ctx,name,port,kind,plug):
    """Run Github Hubot."""

    args = []

    #args = ['-r','specs/%(guid)s/hubot/' % ctx.obj]

    for x in ['basic','views','perso']+[x for x in plug]:
        args += [ '-r' , 'hubot/%s/' % x]

    #args += ['-r','specs/%(guid)s/hubot/' % ctx.obj]

    alias = config('BOT_ALIAS', default="") or ctx.obj['guid']
    token = config('BOT_TOKEN', default="")

    if kind=='telegram':
        os.environ['TELEGRAM_TOKEN'] = token
    elif kind=='slack':
        os.environ['HUBOT_SLACK_TOKEN'] = token
    elif kind=='xmpp':
        os.environ['HUBOT_XMPP_HOST'] = config('BOT_LOBBY',default="irc.freenode.net")
        os.environ['HUBOT_XMPP_ROOMS'] = config('BOT_ROOMS',default="#myhubot-irc")

        os.environ['HUBOT_XMPP_USERNAME'] = alias
        os.environ['HUBOT_XMPP_PASSWORD'] = token
    elif kind=='irc':
        os.environ['HUBOT_IRC_SERVER'] = config('BOT_LOBBY',default="irc.freenode.net")
        os.environ['HUBOT_IRC_ROOMS'] = config('BOT_ROOMS',default="#myhubot-irc")
        os.environ['HUBOT_IRC_NICK'] = alias
        os.environ['HUBOT_IRC_UNFLOOD'] = 'true'

        if len(token):
            os.environ['HUBOT_IRC_USERNAME'] = alias
            os.environ['HUBOT_IRC_PASSWORD'] = token

    os.environ['EXPRESS_PORT'] = str(port)

    os.environ['PARSE_SERVER_LOGS'] = "works/%s/log" % config('APP_KEY')

    shell("hubot","-a",kind,"-n",name or ctx.obj['guid'],*args)

################################################################################

@cli.command('sqldb')
@click.option('--port', default=config('PORT',cast=int,default=7011), help='Port to listen to.')
@click.pass_context
def post_graphile(ctx,port):
    """Run PostGraphile server on PostgreSQL."""

    shell("postgraphile",
        "-p",port,
        "-c",config('DATABASE_URL',default="postgres://localhost:5432/memex")+"?ssl=1",
    "--watch")

#*******************************************************************************

@cli.command('nosql')
@click.option('--name', default=config('APP_KEY',default="xxxxxx"), help='Application name to run with.')
@click.option('--port', default=config('PORT',cast=int,default=7012), help='Port to listen to.')
#@click.option('--serve', default=None, help='BackOffice script to run with.')
#@click.option('--graph', default=None, help='GraphQL schema to run with.')
#@click.option('--front', default='cloud.js', help='FrontOffice script to run with.')
@click.option('--token', default=config('KEY_MASTER',default="xxxxxx"), help='Master Key to run with.')
@click.pass_context
def parse_server(ctx,name,port,token):
    """Run Parse Server on : MongoDB / PostgreSQL."""

    os.environ['PARSE_SERVER_LOGS'] = "works/%s/log" % config('APP_KEY')
    
    link = "http://localhost:%d" % port

    print(' '.join(["parse-server",
        "--appId",name,
        "--databaseURI",config('DATABASE_URL',default="postgres://localhost:5432/memex")+"?ssl=true&rejectUnauthorized=false",
        "--masterKey",token,
        "--clientKey",config('KEY_CLIENT',default=token),
        "--dotNetKey",config('KEY_DOTNET',default=token),
        #"--javascriptKey",config('KEY_REST',default=token),
        "--restAPIKey",config('KEY_REST',default=token),
        "--collectionPrefix","parse_",
        "--port",str(port),
        "--mountPath","/model/cnx",
        "--mountGraphQL","/model/gql",
        "--mountPlayground","/model/iql",
        "--serverURL","%s/model/cnx" % link,
        "--publicServerURL","%s/model/cnx" % link,
    "--verbose"]))

    shell("parse-server",
        "--appId",name,
        "--databaseURI",config('DATABASE_URL',default="postgres://localhost:5432/memex")+"?ssl=true\&rejectUnauthorized=false",
        "--masterKey",token,
        "--clientKey",config('KEY_CLIENT',default=token),
        "--dotNetKey",config('KEY_DOTNET',default=token),
        #"--javascriptKey",config('KEY_REST',default=token),
        "--restAPIKey",config('KEY_REST',default=token),
        "--collectionPrefix","parse_",
        "--port",port,
        "--mountPath","/model/cnx",
        "--mountGraphQL","/model/gql",
        "--mountPlayground","/model/iql",
        "--serverURL","%s/model/cnx" % link,
        "--publicServerURL","%s/model/cnx" % config('APP_URL',default=link),
    "--verbose")

#*******************************************************************************

@cli.command('memex')
@click.option('--port', default=config('PORT',cast=int,default=7013), help='Port to listen to.')
@click.option('--front', default='cloud.js', help='FrontOffice script to run with.')
#@click.option('--serve', default=None, help='BackOffice script to run with.')
#@click.option('--graph', default=None, help='GraphQL schema to run with.')
@click.pass_context
def solid_server(ctx,name,port,front):
    """Run Solid Server on your own"""

    shell("solid-server",
        "-p",port,
        "-c",config('DATABASE_URL',default="postgres://localhost:5432/memex")+"?ssl=1",
        "--appId",port,
        "--masterKey",port,
        "--serverURL",port,
    "--watch")

################################################################################

@cli.command('blues')
@click.option('--port', default=config('PORT',cast=int,default=7003), help='Port to listen to.')
@click.option('--spec', default="think/")
#@click.option('--entail', type=click.Choice(['OWL2RL', 'RDFS'], case_sensitive=False))
#@click.option('--reason', type=click.Choice(['inc', 'tag'], case_sensitive=False))
@click.pass_context
def api_connexion(ctx,port,spec):
    """Run Connexion middleware for Swagger."""

    shell("connexion",spec,
        "--port",port,
    )#"--no-persist")

#*******************************************************************************

@cli.command('proxy')
@click.option('--port', default=config('PORT',cast=int,default=7004), help='Port to listen to.')
@click.pass_context
def gql_rest_proxy(ctx,port):
    """Run graphQL engine using a Web REST proxy."""

    shell("graphql-rest-proxy",
        "--port",port,
        "--baseUrl","%s/graph/web" % config('APP_URL'),
    "graph/proxy.graphql" % ctx.obj)

#*******************************************************************************

@cli.command('daten')
@click.option('--port', default=config('PORT',cast=int,default=7005), help='Port to listen to.')
@click.pass_context
def yaml_server(ctx,port):
    """Run YAML server with static data."""

    shell("yaml-server",
        "--port",port,
        "--database","model/tab/flatten.json" % ctx.obj,
    "--hotReload=on")

################################################################################

@cli.command('think')
@click.option('--port', default=config('PORT',cast=int,default=7006), help='Port to listen to.')
@click.option('--fold','-d', multiple=True, default=[])
@click.pass_context
def dagster_api(ctx,port,fold):
    """Run DAGster API server."""
    
    os.environ['PORT'] = str(port)
    
    os.chdir("queue/dag")
    
    shell("dagit",
        "-h","0.0.0.0",
        "-p",port,
        "-l","/graph/que",
    "--empty-workspace")

#*******************************************************************************

@cli.command('crawl')
@click.option('--port', default=config('PORT',cast=int,default=7007), help='Port to listen to.')
@click.option('--fold','-d', multiple=True, default=[])
@click.pass_context
def scrapy_daemon(ctx,port,fold):
    """Run Scrapy daemon."""
    
    shell("scrapyd")

################################################################################

@cli.command('knows')
@click.option('--name','-n',default="tpf")
@click.option('--title','-t',default="Linked Data Fragments")
@click.option('--port', default=config('PORT',cast=int,default=7008), help='Port to listen to.')
@click.option('--mount','-m',default="/daten/lod")
@click.option('--source','-s',type=click.Choice(['archive','detect','sparql'],case_sensitive=False),default='detect',help="Data sources to append")
@click.option('--folder','-f',multiple=True,default=['/srv/neurox/hdt'],help="Folders to scan within")
@click.option('--worker','-w',type=int,default=2)
@click.pass_context
def ldf_server(ctx,name,title,port,mount,source,folder,worker):
    """Run Linked Data Fragments server for HDT & SparQL."""
    
    prefix = [
        { "prefix": "rdf",         "uri": "http://www.w3.org/1999/02/22-rdf-syntax-ns#" },
        { "prefix": "rdfs",        "uri": "http://www.w3.org/2000/01/rdf-schema#" },
        { "prefix": "xsd",         "uri": "http://www.w3.org/2001/XMLSchema#" },
        { "prefix": "dc",          "uri": "http://purl.org/dc/terms/" },
        { "prefix": "foaf",        "uri": "http://xmlns.com/foaf/0.1/" },
        { "prefix": "dbpedia",     "uri": "http://dbpedia.org/resource/" },
        { "prefix": "dbpedia-owl", "uri": "http://dbpedia.org/ontology/" },
        { "prefix": "dbpprop",     "uri": "http://dbpedia.org/property/" },
        { "prefix": "hydra",       "uri": "http://www.w3.org/ns/hydra/core#" },
        { "prefix": "void",        "uri": "http://rdfs.org/ns/void#" }
    ]
    
    config = {
        "@context": "https://linkedsoftwaredependencies.org/bundles/npm/@ldf/server/^3.0.0/components/context.jsonld",
        "@id": "urn:ldf-server:my",
        "import": "preset-qpf:config-defaults.json",
        "prefixes": prefix,
        "title": title,
        "datasources": [],
    }
    
    dataset = load_yaml('daten/web/datasets.yml')

    if source=='detect':
        pass
    elif source=='archive':
        for entry in dataset['archive']:
            for directory in folder:
                entry['path'] = "%s/%s" % (directory,entry['file'])

                if os.path.exists(entry['path']):
                    config['datasources'].append({
                        "@id": "urn:ldf-server:%(name)s" % entry,
                        "@type": "HdtDatasource",
                        "datasourceTitle": entry['text'],
                        "description": entry['help'],
                        "datasourcePath": entry['name'],
                        "hdtFile": entry['path'],
                    })
    elif source=='sparql':
        for entry in dataset['sparql']:
            config['datasources'].append({
                "@id": "urn:ldf-server:%(name)s" % entry,
                "@type": "SparqlDatasource",
                "datasourceTitle": entry['text'],
                "description": entry['help'],
                "datasourcePath": entry['name'],
                "sparqlEndpoint": entry['link'],
            })
    
    #config['datasources'].append({
    #    "@id": "urn:ldf-server:ghost",
    #    "@type": "HdtDatasource",
    #    "datasourceTitle": entry['text'],
    #    "description": entry['help'],
    #    "datasourcePath": entry['name'],
    #    "hdtFile": entry['path'],
    #})
    
    target = "daten/web/fragment-%s.json" % name

    save_json(config,target)

    shell("ldf-server",target,port,worker)

#*******************************************************************************

@cli.command('vocab')
@click.option('--port', default=config('PORT',cast=int,default=7007), help='Port to listen to.')
@click.option('--folder', default="model/lod/")
@click.option('--entail', type=click.Choice(['OWL2RL', 'RDFS'], case_sensitive=False))
@click.option('--reason', type=click.Choice(['inc', 'tag'], case_sensitive=False))
@click.pass_context
def hylar_owl(ctx,port,folder,entail,reason):
    """Run HyLAR Reasoner for OWL/RDFS."""

    shell("hylar",
        "--port",port,
        "--graph-directory",folder,
        "--entailment",entail,
        "--reasoning-method",reason,
    "--no-persist")

#*******************************************************************************

@cli.command('facts')
@click.option('--port', default=config('PORT',cast=int,default=7008), help='Port to listen to.')
@click.option('--link', default="http://localhost:7001/daten/lod/sparql")
#@click.option('--entail', type=click.Choice(['OWL2RL', 'RDFS'], case_sensitive=False))
#@click.option('--reason', type=click.Choice(['inc', 'tag'], case_sensitive=False))
@click.pass_context
#def trifid(ctx,port,folder,entail,reason):
def trifid(ctx,port,link):
    """Run Trifid reflector with YasGUI."""

    config = {
      "baseConfig": "trifid:config-sparql.json",
      "express": {
        "x-powered-by": None
      },
      "sparqlProxy": {
        "default": {
          "path": "/query"
        }
      },
      "yasgui": {
        "default": {
          "path": "/sparql",
          "urlShortener": "https://s.zazuko.com/api/v1/shorten"
        }
      },
    }

    save_json(config,"nerve/reflect.json")

    shell("trifid",
        "--port",port,
        "--sparql-endpoint-url",link,
    "-v")

################################################################################

@cli.command('admin')
@click.option('--port', default=config('PORT',cast=int,default=7009), help='Port to listen to.')
@click.option('--master', default='9d114265e6044e94', help='Master key for MemexDB instances.')
@click.pass_context
def parse_dashboard(ctx,port,master):
    """Run Parse Dashboard."""
    
    resp = []
    
    for alias,title in {
        'tayaa':          "TAYAA family",
        'juridic':        "Juridic-LoD",
        'hack2use':       "Hack 2 Use",
        'enochian':       "Enochian Designs",
        'maher-ops':      "Maher xOps",
        'connectik':      "Connectik Services",

        'inbijas':        "Initiative INBIJAS",
        'intimae':        "Initiative INTIMAE",
        'oum-noha':       "Cuisine Oum Noha",
        'elbechari':      "El Bechari Space",
        'partcheese':     "PartCheese Fast-Food",
        'cafe-factory':   "Cafe Factory",
        'art-tendance':   "Art Tendance Management",
        'nasmat-taddart': "Nasmat Taddart",
    }.items():
        resp.append({
            "appName": title,

            "appId": alias,
            "masterKey": master,

            "serverURL": "https://%s.herokuapp.com/model/api" % alias,
            "graphQLServerURL": "https://%s.herokuapp.com/model/gql" % alias,
            "iconName": "%s/brand.png" % alias,
        })
        
    save_json({
        'apps': resp,
        'iconsFolder': "asset/"
    },'parse.json')

    if True:
        shell("docker","rm","dash-nosql")

        shell("docker","run","-p","3737:4040","-v","/srv/heroku/end-users/parse.json:/src/Parse-Dashboard/parse-dashboard-config.json","--name","dash-nosql","--restart","always","-d","parseplatform/parse-dashboard","--dev")
    else:
        shell("docker","restart","dash-nosql")

################################################################################

if __name__ == '__main__':
    cli(
        obj={},
    )


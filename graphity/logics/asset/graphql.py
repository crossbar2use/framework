import graphene

from graphene_django.types import DjangoObjectType
from graphene_django.filter import DjangoFilterConnectionField

from .models import *

##########################################################################

class HostingType(DjangoObjectType):
    class Meta:
        model = WebHosting
        filter_fields = ['owner','alias','target']
        interfaces = (graphene.relay.Node, )

#*********************************************************************

class WebSiteType(DjangoObjectType):
    class Meta:
        model = WebSite
        filter_fields  = ['owner','alias','frame','where','target']
        interfaces = (graphene.relay.Node, )

##########################################################################

class AccountType(DjangoObjectType):
    class Meta:
        model = HerokuAccount
        filter_fields  = ['owner','alias','email','token']
        interfaces = (graphene.relay.Node, )

#*********************************************************************

class TemplateType(DjangoObjectType):
    class Meta:
        model = HerokuTemplate
        filter_fields = ['owner','alias']
        interfaces = (graphene.relay.Node, )

#*********************************************************************

class ApplicationType(DjangoObjectType):
    class Meta:
        model = HerokuApplication
        filter_fields = ['owner','alias']
        interfaces = (graphene.relay.Node, )

##########################################################################

class Query(graphene.ObjectType):
    one_hosting = graphene.relay.Node.Field(HostingType)
    all_hosting = DjangoFilterConnectionField(HostingType)

    one_website = graphene.relay.Node.Field(WebSiteType)
    all_website = DjangoFilterConnectionField(WebSiteType)

    one_account = graphene.relay.Node.Field(AccountType)
    all_account = DjangoFilterConnectionField(AccountType)

    one_template = graphene.relay.Node.Field(TemplateType)
    all_template = DjangoFilterConnectionField(TemplateType)

    one_application = graphene.relay.Node.Field(ApplicationType)
    all_application = DjangoFilterConnectionField(ApplicationType)

#*********************************************************************

schema = graphene.Schema(query=Query)


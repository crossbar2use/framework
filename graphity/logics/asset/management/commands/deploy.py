from specs.shortcuts import *

from django.conf import settings
from django.db import connections
from django.utils.version import get_version

class Command(Commandline):
    """
    Example usage:
    python manage.py rqworker high medium low
    """

    args = '<queue queue ...>'

    def add_arguments(self, parser):
        parser.add_argument('--worker-class', action='store', dest='worker_class',
                            help='RQ Worker class to use')
        parser.add_argument('--pid', action='store', dest='pid',
                            default=None, help='PID file to write the worker`s pid into')
        parser.add_argument('--burst', action='store_true', dest='burst',
                            default=False, help='Run worker in burst mode')
        parser.add_argument('--name', action='store', dest='name',
                            default=None, help='Name of the worker')
        parser.add_argument('--queue-class', action='store', dest='queue_class',
                            help='Queues class to use')
        parser.add_argument('--job-class', action='store', dest='job_class',
                            help='Jobs class to use')
        parser.add_argument('--worker-ttl', action='store', type=int,
                            dest='worker_ttl', default=420,
                            help='Default worker timeout to be used')
        parser.add_argument('--sentry-dsn', action='store', default=None, dest='sentry-dsn',
                            help='Report exceptions to this Sentry DSN')

    #****************************************************************************************

    def handle(self, *args, **options):
        for handle,workdir,title in tqdm([
(self.map_deps,"Mapping dependencies"),
#(self.eye_ocr,['eye','ocr','tesseract'],"Tesseract OCR data"),
        ]):
            print "\t%s :" % title

            handle(**options)

    #########################################################################################

    def map_deps(self, **options):
        for bot in tqdm([bot for bot in BotAgent.objects.all()]):
            print "\t*) Bot : %s" % title

            self.ensure_dir('works','heroku',bot.alias, switch=True)

            for key in tqdm(['graph','knows','memex','psych']):
                print "\t\t-> Unit : %s" % title

                self.ensure_git(
                    "file://%s/.git/" % rpath('units',key),
                    'works','heroku',bot.alias,key,
                recursive=True, switch=True)

                link = 'ssh://git@heroku.com/%s-%s-%s.git' % (
                    bot.owner.alias,
                    bot.alias,
                    key,
                )

                self.shell('git','remote','add','heroku',link)

    #****************************************************************************************

    def git_push(self, **options):
        for bot in tqdm([bot for bot in BotAgent.objects.all()]):
            print "\t*) Bot : %s" % title

            for key in tqdm(['graph','knows','memex','psych']):
                print "\t\t-> Unit : %s" % title

                self.ensure_dir('works','heroku',bot.alias,key, switch=True)

                self.shell('git','push','heroku','master')

    #########################################################################################

    def ensure_dir(self, *args, **kwargs):
        pth = rpath(*args)

        if not os.path.exists(pth):
            self.shell('mkdir','-p',pth)

        if kwargs.get('switch',False):
            os.chdir(pth)

        return pth

    #****************************************************************************************

    def ensure_git(self, target, *args, **kwargs):
        pth = rpath(*args)

        if not os.path.exists(pth):
            self.shell('git','clone',target,pth)

        return git.Repo(pth)


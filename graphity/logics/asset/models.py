from django.contrib.auth.models import AbstractUser
from django.db import models

from logics.basic.models import *

#################################################################################

class WebHosting(models.Model):
    owner   = models.ForeignKey(Organism, related_name='hosting')
    alias   = models.CharField(max_length=64)

    target  = models.URLField(blank=True)
    pseudo  = models.CharField(max_length=256,blank=True)
    passwd  = models.CharField(max_length=256,blank=True)

    def __str__(self): return str(self.alias)

WEBSITE_TYPEs = (
    ('wp', "WordPress"),

    ('mg', "Magento"),
    ('ps', "PrestaShop"),
)

class WebSite(models.Model):
    owner   = models.ForeignKey(Identity, related_name='websites')
    alias   = models.CharField(max_length=64)

    frame   = models.CharField(max_length=24,choices=WEBSITE_TYPEs,blank=True)
    where   = models.ForeignKey(WebHosting, related_name='websites')

    target  = models.URLField(blank=True)
    pseudo  = models.CharField(max_length=256,blank=True)
    passwd  = models.CharField(max_length=256,blank=True)

    is_gql  = models.BooleanField(default=True)

    def __str__(self): return str(self.alias)

#################################################################################

class HerokuAccount(models.Model):
    owner   = models.ForeignKey(Organism, related_name='platforms')
    alias   = models.CharField(max_length=64, blank=True)

    email   = models.CharField(max_length=256)
    token   = models.CharField(max_length=256,blank=True)

    def __str__(self): return str(self.alias)

class HerokuBuildpack(models.Model):
    name   = models.CharField(max_length=64)
    link   = models.URLField(blank=True)

    def __str__(self): return str(self.name)

class HerokuTemplate(models.Model):
    owner   = models.ForeignKey(Organism, related_name='templates')
    alias   = models.CharField(max_length=64)
    build   = models.ManyToManyField(HerokuBuildpack, related_name='templates', blank=True)

    rname   = models.CharField(max_length=64,blank=True)
    rlink   = models.URLField(blank=True)
    categ   = models.CharField(max_length=64,blank=True)

    gh_user = models.CharField(max_length=64)
    gh_repo = models.CharField(max_length=64)

    def __str__(self): return str(self.alias)

class HerokuApplication(models.Model):
    owner   = models.ForeignKey(HerokuAccount, related_name='applets')
    alias   = models.CharField(max_length=64)

    build   = models.ManyToManyField(HerokuBuildpack, related_name='applications', blank=True)

    rlink   = models.URLField(blank=True)
    local   = models.CharField(max_length=256,blank=True)

    def __str__(self): return str(self.alias)


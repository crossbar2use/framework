from django.contrib import admin

from .models import *

######################################################################

class HostingAdmin(admin.ModelAdmin):
    list_display  = ['owner','alias','target']
    list_filter   = ['owner__name']
    #list_editable = []

admin.site.register(WebHosting, HostingAdmin)

class WebSiteAdmin(admin.ModelAdmin):
    list_display  = ['owner','alias','frame','where','target']
    list_filter   = ['owner__email','frame','where__alias']
    list_editable = ['target']

admin.site.register(WebSite, WebSiteAdmin)

######################################################################

class AccountAdmin(admin.ModelAdmin):
    list_display  = ['owner','alias','email','token']
    list_filter  = ['owner__name']
    list_editable = ['email','token']

admin.site.register(HerokuAccount, AccountAdmin)

class BuildpackAdmin(admin.ModelAdmin):
    list_display  = ['name','link']
    #list_filter   = ['owner']
    list_editable = ['link']

admin.site.register(HerokuBuildpack, BuildpackAdmin)

class TemplateAdmin(admin.ModelAdmin):
    list_display  = ['owner','alias','rname','rlink','categ','gh_user','gh_repo']
    list_filter   = ['alias','rname','rlink','categ']
    #list_editable = []

admin.site.register(HerokuTemplate, TemplateAdmin)

class ApplicationAdmin(admin.ModelAdmin):
    #list_display  = []
    list_filter   = ['owner']
    #list_editable = []

admin.site.register(HerokuApplication, ApplicationAdmin)


import graphene

from graphene_django.types import DjangoObjectType
from graphene_django.filter import DjangoFilterConnectionField

from .models import *

######################################################################

class VocabularType(DjangoObjectType):
    class Meta:
        model = Vocabular
        filter_fields = ['owner','alias','frame','rlink']
        interfaces = (graphene.relay.Node, )

#*********************************************************************

#class DatasetType(DjangoObjectType):
#    class Meta:
#        model = Dataset
#        filter_fields = ['owner','alias','frame','rlink']
#        interfaces = (graphene.relay.Node, )

#*********************************************************************

class CorporaType(DjangoObjectType):
    class Meta:
        model = Corpora
        filter_fields = ['owner','alias','frame','rlink','speak']
        interfaces = (graphene.relay.Node, )

######################################################################
######################################################################
######################################################################

class MotifType(DjangoObjectType):
    class Meta:
        model = VisualMotif
        filter_fields = ['alias','flags','cv_id']
        interfaces = (graphene.relay.Node, )

#*********************************************************************

class RecogType(DjangoObjectType):
    class Meta:
        model = OpticalRecog
        filter_fields = ['owner','alias','frame','rlink']
        interfaces = (graphene.relay.Node, )

######################################################################

class ModelType(DjangoObjectType):
    class Meta:
        model = VisualModel
        filter_fields = ['owner','alias','frame','model','confs']
        interfaces = (graphene.relay.Node, )

#*********************************************************************

class OrganType(DjangoObjectType):
    class Meta:
        model = VisualOrgan
        filter_fields = ['owner','alias','frame','model']
        interfaces = (graphene.relay.Node, )

######################################################################

class SightQuery(graphene.ObjectType):
    one_visual_motif = graphene.relay.Node.Field(MotifType)
    all_visual_motif = DjangoFilterConnectionField(MotifType)

    one_visual_recog = graphene.relay.Node.Field(RecogType)
    all_visual_recog = DjangoFilterConnectionField(RecogType)

    one_visual_model = graphene.relay.Node.Field(ModelType)
    all_visual_model = DjangoFilterConnectionField(ModelType)

    one_visual_organ = graphene.relay.Node.Field(OrganType)
    all_visual_organ = DjangoFilterConnectionField(OrganType)

#*********************************************************************

class TongsQuery(graphene.ObjectType):
    one_vocabular = graphene.relay.Node.Field(VocabularType)
    all_vocabular = DjangoFilterConnectionField(VocabularType)

    one_corpora = graphene.relay.Node.Field(CorporaType)
    all_corpora = DjangoFilterConnectionField(CorporaType)

#*********************************************************************

class Query(SightQuery,TongsQuery):
    pass

#*********************************************************************

schema = graphene.Schema(query=Query)


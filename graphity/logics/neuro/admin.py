from django.contrib import admin

from .models import *

######################################################################

class VocabularAdmin(admin.ModelAdmin):
    list_display  = ['owner','alias','frame','rlink']
    list_filter  = ['frame','owner__email'] #,'speak__name']
    list_editable = ['frame','rlink']

admin.site.register(Vocabular, VocabularAdmin)

#*********************************************************************

class DatasetAdmin(admin.ModelAdmin):
    list_display  = ['owner','alias','frame','rlink']
    list_filter  = ['frame','owner__email','speak__name']
    list_editable = ['frame','rlink']

#admin.site.register(Dataset, DatasetAdmin)

######################################################################

class CorporaAdmin(admin.ModelAdmin):
    list_display  = ['owner','alias','frame','rlink','speak']
    list_filter  = ['frame','owner__email','speak__name']
    list_editable = ['frame','rlink','speak']

admin.site.register(Corpora, CorporaAdmin)

######################################################################
######################################################################
######################################################################
######################################################################

class MotifAdmin(admin.ModelAdmin):
    list_display  = ['alias','flags','cv_id']
    list_editable = ['flags','cv_id']

admin.site.register(VisualMotif, MotifAdmin)

#*********************************************************************

class RecogAdmin(admin.ModelAdmin):
    list_display  = ['owner','alias','frame','rlink']
    list_filter  = ['frame','owner__email']
    list_editable = ['frame','rlink']

admin.site.register(OpticalRecog, RecogAdmin)

#*********************************************************************

class ModelAdmin(admin.ModelAdmin):
    list_display  = ['owner','alias','frame','model','confs']
    list_filter  = ['owner__email','frame']
    list_editable = ['frame','model','confs']

admin.site.register(VisualModel, ModelAdmin)

#*********************************************************************

class OrganAdmin(admin.ModelAdmin):
    list_display  = ['owner','alias','frame','model']
    list_filter  = ['frame','owner__name','model__frame','model__alias']
    list_editable = ['frame','model']

admin.site.register(VisualOrgan, OrganAdmin)


from specs.shortcuts import *

from django.conf import settings
from django.db import connections
from django.utils.version import get_version

class Command(Commandline):
    """
    Runs RQ workers on specified queues. Note that all queues passed into a
    single rqworker command must share the same connection.

    Example usage:
    python manage.py rqworker high medium low
    """

    args = '<queue queue ...>'

    def add_arguments(self, parser):
        parser.add_argument('--worker-class', action='store', dest='worker_class',
                            help='RQ Worker class to use')
        parser.add_argument('--pid', action='store', dest='pid',
                            default=None, help='PID file to write the worker`s pid into')
        parser.add_argument('--burst', action='store_true', dest='burst',
                            default=False, help='Run worker in burst mode')
        parser.add_argument('--name', action='store', dest='name',
                            default=None, help='Name of the worker')
        parser.add_argument('--queue-class', action='store', dest='queue_class',
                            help='Queues class to use')
        parser.add_argument('--job-class', action='store', dest='job_class',
                            help='Jobs class to use')
        parser.add_argument('--worker-ttl', action='store', type=int,
                            dest='worker_ttl', default=420,
                            help='Default worker timeout to be used')
        parser.add_argument('--sentry-dsn', action='store', default=None, dest='sentry-dsn',
                            help='Report exceptions to this Sentry DSN')

    #************************************************************************************

    def handle(self, *args, **options):
        print "Configuring scripts :"

        #print "\t-> Parse Dashboard"

        self.write_json([],'external-scripts.json')

        #print "\t-> Linked Data Fragments"

        self.write_json([],'hubot-scripts.json')

        #print "\t-> Linked Data Fragments"

        self.shell('node_modules/.bin/hubot',"-a","$BOT_FRAME","-n","$BOT_ALIAS",
            "-r","logics/basic/dialects",
            "-r","logics/cloud/dialects",
            "-r","logics/daten/dialects",
            "-r","logics/opsys/dialects",
            "-r","logics/sight/dialects",
            "-r","logics/tongs/dialects",
        "-r","works/agent/")


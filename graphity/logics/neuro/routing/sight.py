from django.conf.urls import include, url

from . import views

urlpatterns = [
    url(r'^$',                        views.HomeView.as_view(), name='home'),
    url(r'^organ/(?P<alias>.*)',      views.OrganView.as_view(), name="organ_view"),
    #url(r'^organ/(?P<alias>.*)/deploy', views.GenerateView.as_view(), name="organ_detect"),
]


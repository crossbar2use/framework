from django.views.generic import TemplateView
from django.contrib.auth import logout, get_user_model
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import ensure_csrf_cookie
from rest_framework import generics, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response
from django.template import loader
from django.http import HttpResponse

from .models import VisualOrgan

#***********************************************************************

class HomeView(TemplateView):
    template_name = 'sight/home.html'

    def get(self, request, *args, **kwargs):
        return Response(status=status.HTTP_204_NO_CONTENT)

#***********************************************************************

class OrganView(TemplateView):
    template_name = 'sight/organ.html'

    def get(self, request, alias, *args, **kwargs):
        eye = self.instance(alias)

        if eye is not None:
            print "Selected organ : %s" % eye

            return super(OrganView, self).get(request, *args, **kwargs)
        else:
            raise Http404()

    #@method_decorator(ensure_csrf_cookie)
    def post(self, request, alias, *args, **kwargs):
        eye = self.instance(alias)

        if eye is not None:
            file = Image.open(request.files['file'].stream)

            img = eye.detect(file)

            return send_file(io.BytesIO(img),attachment_filename='image.jpg',mimetype='image/jpg')
        else:
            raise Http404()

        return Response(status=status.HTTP_204_NO_CONTENT)
    
    def instance(self, alias):
        try:
            return VisualOrgan.objects.get(alias=alias)
        except KeyError,ex:
            return None

#***********************************************************************

class GenerateView(APIView):
    def get(self, request, *args, **kwargs):
        return Response(status=status.HTTP_204_NO_CONTENT)

    def post(self, request, *args, **kwargs):
        return Response(status=status.HTTP_204_NO_CONTENT)


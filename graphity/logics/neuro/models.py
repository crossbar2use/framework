from django.db import models

from logics.basic.models import *

#import cv2 as cv
#import numpy
#from PIL import Image

######################################################################

LINKED_TYPEs = (
    ('rdf', "Rich Description Framework"),
    ('owl', "Ontology Web Language"),
    ('lod', "JSON-LD context"),
    ('hdt', "Header Dictionary Table"),
)

class Vocabular(models.Model):
    owner    = models.ForeignKey(Identity, related_name='vocabs')
    alias    = models.CharField(max_length=128)

    rlink    = models.URLField(blank=True)
    graph    = models.CharField(max_length=128,blank=True)

    frame    = models.CharField(max_length=24, choices=LINKED_TYPEs)

    class Meta:
        verbose_name = "Vocabular"
        verbose_name_plural = "Vocabulars"

    def __str__(self): return str(self.alias)

#*********************************************************************

LEXER_TYPEs = (
    ('nltk', "Natural-Language Tool-Kit"),
    ('core', "Stanford CoreNLP corporas"),

    ('cn5', "ConceptNet 5 files"),
    ('vcr', "Visual Common Sense"),

    ('nlp', "Natural Language Processing"),
)

class Corpora(models.Model):
    owner    = models.ForeignKey(Identity, related_name='corporas')
    alias    = models.CharField(max_length=128)
    frame    = models.CharField(max_length=24, choices=LEXER_TYPEs)

    rname    = models.CharField(max_length=128,blank=True)
    rlink    = models.URLField(blank=True)
    speak    = models.ForeignKey(Language, related_name='corporas',null=True,blank=True)

    class Meta:
        verbose_name = "Corpora"
        verbose_name_plural = "Corporas"

    def __str__(self): return str(self.alias)

######################################################################
######################################################################
######################################################################
######################################################################
######################################################################

class VisualMotif(models.Model):
    alias      = models.CharField(max_length=128)
    flags      = models.CharField(max_length=64, blank=True)

    cv_id      = models.IntegerField(blank=True)

    def __str__(self): return str(self.alias)

#*********************************************************************

RECOG_TYPEs = (
    ('ts',  "Google's Tesseract"),
)

class OpticalRecog(models.Model):
    owner      = models.ForeignKey(Identity, related_name='recogns')
    alias      = models.CharField(max_length=64)

    frame      = models.CharField(max_length=24, choices=RECOG_TYPEs)
    rname      = models.CharField(max_length=128,blank=True)
    rlink      = models.URLField(blank=True)

    def __str__(self): return str(self.alias)

######################################################################

MODEL_TYPEs = (
    ('cv',  "Open Computer Vision"),
    ('mz',  "Open Model Zoo"),
    ('tf',  "Tensor Flow"),
)

class VisualModel(models.Model):
    owner      = models.ForeignKey(Identity, related_name='visuals')
    alias      = models.CharField(max_length=64)

    frame      = models.CharField(max_length=24, choices=MODEL_TYPEs)

    model      = models.CharField(max_length=128,blank=True)
    confs      = models.CharField(max_length=128,blank=True)

    def __str__(self): return str(self.alias)

#*********************************************************************

ORGAN_TYPEs = (
    ('cv',  "Open CV"),
    ('tf',  "Tensor Flow"),
)

class VisualOrgan(models.Model):
    owner      = models.ForeignKey(Organism, related_name='visuals')
    alias      = models.CharField(max_length=128, blank=True)

    frame      = models.CharField(max_length=24, choices=ORGAN_TYPEs)
    model      = models.ForeignKey(VisualModel, related_name='organs')

    def __str__(self): return str(self.alias)

    @property
    def instance(self):
        if self.frame=='cv':
            if self.model.frame=='tf':
                mdl = rpath('daten','eye','cv2','tensorflow',self.model.model)
                cfg = rpath('daten','eye','cv2','tensorflow',self.model.confs)

                return cv.dnn.readNetFromTensorflow(mdl,cfg)

        return None

    def detect(self,entry,width=300,height=300):
        if self.frame=='cv':
            cvNet = self.instance

            img = cv.cvtColor(numpy.array(entry), cv.COLOR_BGR2RGB)

            cvNet.setInput(cv.dnn.blobFromImage(img, 0.007843, (width,height), (127.5, 127.5, 127.5), swapRB=True, crop=False))

            detections = cvNet.forward()

            cols = img.shape[1]
            rows = img.shape[0]

            for i in range(detections.shape[2]):
                confidence = detections[0, 0, i, 2]
                if confidence > 0.5:
                    class_id = int(detections[0, 0, i, 1])

                    xLeftBottom = int(detections[0, 0, i, 3] * cols)
                    yLeftBottom = int(detections[0, 0, i, 4] * rows)
                    xRightTop = int(detections[0, 0, i, 5] * cols)
                    yRightTop = int(detections[0, 0, i, 6] * rows)

                    cv.rectangle(img, (xLeftBottom, yLeftBottom), (xRightTop, yRightTop),
                                 (0, 0, 255))
                    if class_id in classNames:
                        label = classNames[class_id] + ": " + str(confidence)
                        labelSize, baseLine = cv.getTextSize(label, cv.FONT_HERSHEY_SIMPLEX, 0.5, 1)
                        yLeftBottom = max(yLeftBottom, labelSize[1])
                        cv.putText(img, label, (xLeftBottom+5, yLeftBottom), cv.FONT_HERSHEY_SIMPLEX, 0.5, (255,255,255))

            img = cv.imencode('.jpg', img)[1].tobytes()

            return img
        else:
            pass

        return None


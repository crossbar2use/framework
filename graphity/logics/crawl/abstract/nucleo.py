from abstract import *

#*******************************************************************************

class BaseExporter(Helper):
    def initialize(self, *args, **kwargs):
        self.trigger('prepare', *args, **kwargs)

        site = 'adala.justice.gov.ma' # for site in Reactor.config.get('site',[]):

        self.trigger('connect', *args, **kwargs)

        self._map = {}

        self.trigger('remixes', *args, **kwargs)

    def rewires(self, data_type, handler):
        if data_type not in self._map:
            self._map[data_type] = []

        if handler not in self._map[data_type]:
            self._map[data_type].append(handler)

        return handler

    def execute(self):
        for data_type in self._map:
            for handler in self._map[data_type]:
                data = open(rpath('dataset',data_type,'%s.jsonl' % site)).read()

                for line in (data.split('\n')):#tqdm
                    item = json.loads(line)

                    for key in ('name','unit'):
                        while '\n' in item[key]:
                            item[key] = item[key].replace('\n',' ')

                        item[key] = item[key].strip()

                        #item[key] = unicode(item[key]).encode('utf-8')

                    self.trigger('process', context, item, **)

################################################################################

@singleton()
class Reactor(Helper):
    def initialize(self):
        self._cfg = {}
        self._dns = 'local'

        self._dsl = {}
        self._rel = {}
        self._map = {}
        self._spy = {}

        self._rds = Redis()
        self._que = {}

    config = property(lambda self: self._cfg)
    domain = property(lambda self: self._dns)

    #***************************************************************************

    def setup(self, domain, **config):
        self._cfg = config
        self._dns = domain

        for subfolder in [
            rpath('cachier'),
            rpath('origins'),
            rpath('cachier',domain),
            rpath('origins',domain),
        ]:
            if not os.path.exists(rpath(subfolder)):
                os.mkdir(rpath(subfolder))

    #***************************************************************************

    def shell(self, program, *arguments):
        stmt = []

        for part in arguments:
            if ' ' in part:
                part = '"%s"'  % part

            stmt.append(part)

        stmt = ' '.join([program] + stmt)

        os.system(stmt)

    ############################################################################

    def model(self, *args, **kwargs):
        def do_apply(handler, alias, **options):
            if alias not in self._rel:
                self._rel[alias] = handler(None, alias)

            for subfolder in [
                rpath('dataset',alias),
                rpath('mapping',alias),
            ]:
                if not os.path.exists(subfolder):
                    os.mkdir(subfolder)

            return self._rel[alias]

        return lambda hnd: do_apply(hnd, *args, **kwargs)

    models = property(lambda self: self._rel)

    def hooks(self, *args, **kwargs):
        def do_apply(handler, model, **options):
            for alias in model:
                if alias not in self._map:
                    self._map[alias] = []

                if handler not in self._map[alias]:
                    self._map[alias].append(handler)

            return handler

        return lambda hnd: do_apply(hnd, *args, **kwargs)

    def entity(self, alias, narrow, **payload):
        if alias in self._rel:
            return self._rel[alias].create(narrow,**payload)
        else:
            raise KeyError("Model '%s' not found !" % alias)

    ############################################################################

    queues = property(lamba self: self._que)

    #***************************************************************************

    def queue(self, alias):
        if alias not in self._que:
            self._que[alias] = Queue(alias, connection=self._rds)

        return self._que[alias]

    #***************************************************************************

    def task(self, *args, **kwargs):
        return lambda hnd: rq_job(*args, **kwargs)(hnd)

    #***************************************************************************

    def enqueue(self, alias, narrow, *arg, **kwarg):
        return self.queue(alias).enqueue(narrow, *arg, **kwarg)

    #***************************************************************************

    def dequeue(self, alias):
        raise NotImplemented()

    ############################################################################

    def spider(self, handle, target, caller, **config):
        cnt = Context(handle,target)

        for entry in handle(cnt, **config):
            cnt._item.append(entry)

        #print self.handle, self.xpath('//*'), payload

        for entry in tqdm(cnt.result):#tqdm
            if callable(caller):
                caller(self, entry, **config)
            else:
                item.save()

        #cnt.process(**config)

    def crawl(self, *args, **kwargs):
        def do_apply(handler, target, **options):
            click.echo('\t*) %s' % target)

            self.spider(handler, target, None, **options)

            return handler

        return lambda hnd: do_apply(hnd, *args, **kwargs)

################################################################################

class ParseExporter(BaseExporter):
    def prepare(self, *args, **kwargs):
        self._cnx = None
        self._map = {}

        self.trigger('connect', *args, **kwargs)

    #*********************************************************************************

    site = property(lambda self: self._cnx)

    #*********************************************************************************

    

    ##################################################################################


#*******************************************************************************

class WordPressExporter(BaseExporter):
    def prepare(self, *args, **kwargs):
        self._cnx = None

    #*********************************************************************************

    site = property(lambda self: self._cnx)

    #*********************************************************************************

    def connect(self, *args, **kwargs):
        self._cnx = Client('%/xmlrpc.php' % self.WP_uer, self.WP_uer, self.WP_uer)

    ##################################################################################

    def read_post(self,kind,name):
        lst = self.site.call(GetPosts(kind))

        for post in lst:
            if post.slug==name:
                return post

        return None

    #*********************************************************************************

    def ensure_post(self,kind,name,**extra):
        post = self.read_post(kind,name)

        if post is not None:
            return post

        return self.create_post(kind,name,**extra)

    #*********************************************************************************

    def create_post(self,kind,name,**extra):
        post = WordPressPost()

        post.slug = name
        post.title = extra.get('title',"") or name

        post.excerpt = extra.get('excerpt',"") or name
        post.content = extra.get('content',"") or name

        post.post_type = kind
        post.post_status = 'publish' # draft

        post.id = self.site.call(NewPost(post))

        return post

    ##################################################################################

    def read_term(self,taxo,name):
        lst = self.site.call(WordPressTaxonomies.GetTerms(taxo))

        for tag in lst:
            if tag.name==name:
                return tag

        return None

    #*********************************************************************************

    def ensure_term(self,taxo,name,**extra):
        post = self.read_term(taxo,name)

        if post is not None:
            return post

        return self.create_term(taxo,name,**extra)

    #*********************************************************************************

    def create_term(self,taxo,name,**extra):
        tag = WordPressTerm()

        tag.taxonomy = taxo
        tag.name = name

        tag.id = self.site.call(WordPressTaxonomies.NewTerm(tag))

        return tag


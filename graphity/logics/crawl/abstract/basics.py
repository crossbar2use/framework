from helpers import *

################################################################################

class Helper(object):
    def __init__(self, *args, **kwargs):
        self.trigger('initialize', *args, **kwargs)

    def trigger(self, method, *args, **kwargs):
        handler = getattr(self, method, None)

        if callable(handler):
            return handler(*args, **kwargs)

#*********************************************************************

class Record(Helper):
    def initialize(self, factory, primary, payload={}):
        self._prn = factory
        self._nrw = primary
        self._raw = payload

        for f in factory.fields.values():
            value = payload.get(f.alias,f.default)

            setattr(self,f.alias,value)

    #parent = property(lambda self: self._prn)
    #narrow = property(lambda self: self._nrw)
    #fields = property(lambda self: self._raw)

    hashed = property(lambda self: md5(self._nrw.encode('utf-8','ignore')).hexdigest())

    rpath = lambda self,*x : rpath('mapping',self._prn.alias,self.hashed,*x)

    def save(self):
        pth = rpath('dataset',self._prn.alias,'%s.jsonl' % Reactor.domain)

        with codecs.open(pth,'a+',encoding='utf-8') as f:
            f.write("%s\n" % json.dumps(self.__json__()))

        if self._nrw is not None:
            if not os.path.exists(self.rpath()):
                os.mkdir(self.rpath())

            with open(self.rpath('narrow.txt'),'w+') as f:
                f.write(self._nrw.encode('utf-8','ignore'))

            with open(self.rpath('crawled.json'),'w+') as f:
                f.write(json.dumps(self.__json__(), indent=4))

            if False and hasattr(self._prn,'RDF_TYPE') and hasattr(self._prn,'RDF_LINK'):
                g = RdfGraph()

                for triple in self.__rdflib__:
                    g.add(triple)

                with open(self.rpath('resource.ttl'),'w+') as f:
                    raw = g.serialize(format='turtle')

                    f.write(raw)

            if hasattr(self._prn,'JSON_LD'):
                with open(self.rpath('context.jsonld'),'w+') as f:
                    f.write(json.dumps(self.__jsonld__(), indent=4))

    #***************************************************************************

    def __json__(self):
        return dict([(f.alias,f.extracts(self)) for f in self._prn.fields.values()])

    def __jsonld__(self):
        doc = {}

        for f in self._prn.fields.values():
            if f.alias in self._prn.JSON_LD:
                cfg = self._prn.JSON_LD[f.alias]

                if type(cfg) is dict:
                    doc[cfg['@id']] = {
                        cfg['@type'] : f.extracts(self),
                    }
                else:
                    doc[cfg] = f.extracts(self)

        return jsonld.compact(doc, self._prn.JSON_LD)

    #***************************************************************************

    def __rdflib__(self):
        res = BNode()

        if self._nrw is not None:
            res = URIRef(self._prn.RDF_LINK % self._nrw)

        yield res, RdfNS.RDF.Type, self._prn.RDF_TYPE

        for f in self._prn.fields.values():
            if f.rdf_ns is not None:
                yield res, f.rdf_ns, Literal(f.extracts(self))

    def __owllib__(self):
        onto = None

        return onto

################################################################################

class Objet(Helper):
    def initialize(self, parent, alias):
        self._obj = parent
        self._key = alias
        self._col = {}

        for key in dir(self):
            hnd = getattr(self,key,None)

            if isinstance(hnd,Field):
                hnd._obj = self
                hnd._key = key

                self._col[key] = hnd

    cursor = property(lambda self: self._obj)
    alias  = property(lambda self: self._key)
    fields = property(lambda self: self._col)

    def validate(self, record):
        return reduce(operator.and_, [f.validate(f.extracts(record)) for f in self.fields.values()] or [True])

    def extracts(self, record):
        if type(record) in [dict]:
            return record.get(self.alias, self.default)
        else:
            return getattr(record, self.alias, self.default)

    def create(self, narrow, **payload):
        resp = self.Wrapper(self,narrow,payload)

        return resp

#*******************************************************************************

class Field(Helper):
    def initialize(self, xsd, rdf=None):
        self._def = None

        self._xsd = xsd
        self._rdf = rdf

    objet = property(lambda self: self._obj)
    alias = property(lambda self: self._key)

    default = property(lambda self: self._def)

    xsd_type = property(lambda self: self._xsd)
    rdf_type = property(lambda self: self._rdf)

    def validate(self, value):
        return True

    def extracts(self, record):
        return getattr(record, self.alias, self.default)

################################################################################

class Context(Helper):
    def initialize(self, handle, target):
        self._func = handle
        self._link = target

        self._opts = {}
        self._path = None
        self._item = []

    handle = property(lambda self: self._func)
    target = property(lambda self: self._link)
    config = property(lambda self: self._opts)

    cpath = lambda self,*x: rpath('cachier',Reactor.domain,*x)
    dpath = lambda self,*x: rpath('dataset',Reactor.domain,*x)
    spath = lambda self,*x: rpath('origins',Reactor.domain,*x)

    hashed = property(lambda self: md5(self.target).hexdigest())

    @property
    def content(self):
        response = requests.get(self.target)

        if response.status_code == 200:
            soup = BeautifulSoup(response.content, 'html.parser')

            return soup.prettify()

    @property
    def cached(self):
        pth = self.cpath(self.hashed)

        if not os.path.exists(pth):
            response = requests.get(self.target)

            if response.status_code == 200:
                with codecs.open(pth,'wb+',encoding='utf-8') as f:
                    text = response.content.decode('ascii', 'ignore')

                    soup = BeautifulSoup(text, 'html.parser')

                    f.write(soup.prettify())

        if os.path.exists(pth):
            return open(pth).read()

        #return ""

    result = property(lambda self: self._item)

    @property
    def selek(self):
        if self._path is None:
            self._path = Selector(unicode(self.content))

        return self._path

    def css(self, *args, **kwargs):   return self.selek.css(*args, **kwargs)
    def xpath(self, *args, **kwargs): return self.selek.xpath(*args, **kwargs)

    def process(self, **options):
        for item in tqdm(cnt.result):#tqdm
            for narrow in self._map:
                factory = self._rel[narrow]

                if isinstance(item,tyep(factory)):
                    for callback in self._map[narrow]:
                        if callable(callback):
                            yield callback(cnt, item, **config)
                        else:
                            print callback
                else:
                    print "inherit",item,factory


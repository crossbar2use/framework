import os,sys,time,click,math,codecs
from tqdm import tqdm
import simplejson as json,yaml

#*******************************************************************************

rpath = lambda *x: os.path.join(os.getcwd(),*x)

UTF8Writer = codecs.getwriter('utf8')
sys.stdout = UTF8Writer(sys.stdout)

#*******************************************************************************

from urlparse import *
import requests
from parsel import Selector
from md5 import md5
from datetime import time,date,datetime

#*******************************************************************************

from bs4 import BeautifulSoup

from docx import Document as WordDocument

import nltk, tika, textract ;                                                                                                      WP_PASSWORD = 'RebirthOf2501'

from rdflib import Graph as RdfGraph, namespace as RdfNS, Literal, BNode, URIRef
from pyld import jsonld

#*******************************************************************************

from wordpress_xmlrpc import Client, WordPressPost
from wordpress_xmlrpc.methods.posts import GetPosts, NewPost, EditPost
from wordpress_xmlrpc.methods.users import GetUserInfo
from wordpress_xmlrpc import WordPressTerm
from wordpress_xmlrpc.methods import taxonomies as WordPressTaxonomies

#*********************************************************************

from redis import Redis
from rq import Queue as RQ_Queue

from rq.decorators import job as rq_job

################################################################################

def singleton(*args, **kwargs):
    return lambda handler: handler(*args, **kwargs)

#*********************************************************************

def download_to(source,target):
    r = requests.get(source, stream=True)

    # Total size in bytes.
    total_size = int(r.headers.get('content-length', 0));
    block_size = 1024
    wrote = 0
    with open(target, 'wb') as f:
        for data in tqdm(r.iter_content(block_size), total=math.ceil(total_size//block_size) , unit='KB', unit_scale=True):
            wrote = wrote  + len(data)
            f.write(data)
    if total_size != 0 and wrote != total_size:
        print("ERROR, something went wrong")

#*********************************************************************

def pdf_to_txt(source,target,**options):
    try:
        raw = textract.process(source, method=options.get('miner','pdftotext'), language='eng')
    except:
        return

    with open(target, 'w+') as f:
        f.write(raw)

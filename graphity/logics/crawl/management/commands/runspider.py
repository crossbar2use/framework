from specs.shortcuts import *

from django.conf import settings
from django.db import connections
from django.utils.version import get_version

class Command(Commandline):
    """
    Runs RQ workers on specified queues. Note that all queues passed into a
    single rqworker command must share the same connection.

    Example usage:
    python manage.py rqworker high medium low
    """

    args = '<alias ...>'

    def add_arguments(self, parser):
        parser.add_argument('alias', action='store',
                            help='RQ Worker class to use')

        parser.add_argument('--download', action='store_true', dest='download',
                            default=False, help='Download static files.')
        parser.add_argument('--shallow', action='store_false', dest='download',
                            default=False, help='Download static files.')

        parser.add_argument('--miner','-m', action='store', dest='miner',
                            default='pdftotext',help='RQ Worker class to use')

        return

        parser.add_argument('--pid', action='store', dest='pid',
                            default=None, help='PID file to write the worker`s pid into')

        parser.add_argument('--name', action='store', dest='name',
                            default=None, help='Name of the worker')
        parser.add_argument('--queue-class', action='store', dest='queue_class',
                            help='Queues class to use')
        parser.add_argument('--job-class', action='store', dest='job_class',
                            help='Jobs class to use')
        parser.add_argument('--worker-ttl', action='store', type=int,
                            dest='worker_ttl', default=420,
                            help='Default worker timeout to be used')
        parser.add_argument('--sentry-dsn', action='store', default=None, dest='sentry-dsn',
                            help='Report exceptions to this Sentry DSN')

    #*******************************************************************************************

    def wrapper(self, domain, *args, **opts):
        nspace = domain.reverse()

        Reactor.setup(domain)

        spy = __import__('spiders.%s' % nspace)

        print spy ; exit(1)

        click.echo('Running crawler against : %s' % domain)

        exec(open(rpath('spiders',domain)).read(),globals(),locals())

    #*******************************************************************************************

    def process(self, *args, **options):
        print "Configuring scripts :"

        #print "\t-> Parse Dashboard"

        self.write_json([],'external-scripts.json')

        #print "\t-> Linked Data Fragments"

        self.write_json([],'hubot-scripts.json')

        #print "\t-> Linked Data Fragments"

        self.shell('node_modules/.bin/hubot',"-a","$BOT_FRAME","-n","$BOT_ALIAS",
            "-r","logics/basic/dialects",
            "-r","logics/cloud/dialects",
            "-r","logics/daten/dialects",
            "-r","logics/opsys/dialects",
            "-r","logics/sight/dialects",
            "-r","logics/tongs/dialects",
        "-r","works/agent/")

    ############################################################################################

    def handle(self, alias, *args, **options):
        #self.wrapper(*args, **options)

        code = rpath('crawl','%s.py' % alias)
        data = rpath('works','crawler','%s.json' % alias)

        self.shell('scrapy','runspider',code,'-o',data)


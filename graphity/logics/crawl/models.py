from django.contrib.auth.models import AbstractUser
from django.db import models

from logics.basic.models import *
from logics.daten.models import *

#################################################################################

class Realm(models.Model):
    owner    = models.ForeignKey(Identity, related_name='realms')
    alias    = models.CharField(max_length=64)

    domains  = models.TextField(blank=True)

    #typed   = models.ForeignKey(CustomType, related_name='fields',null=True,blank=True)

#********************************************************************************

class Spider(models.Model):
    realm    = models.ForeignKey(Realm, related_name='spiders')
    alias    = models.CharField(max_length=64)

    start    = models.TextField(blank=True)

#################################################################################

class Pattern(models.Model):
    spider    = models.ForeignKey(Spider, related_name='patterns')

    allows    = models.CharField(max_length=64)
    reject    = models.CharField(max_length=64)

    follow    = models.BooleanField(default=False)
    handle    = models.TextField(blank=True)

#********************************************************************************

class Resource(models.Model):
    owner    = models.ForeignKey(Spider, related_name='resources')
    alias    = models.CharField(max_length=64)

    rlink    = models.URLField()
    mimes    = models.CharField(max_length=256,blank=True)

    vtype    = models.ForeignKey(CustomType, related_name='resources',null=True,blank=True)

    items    = models.ManyToManyField(CustomEntity, related_name='resources',blank=True)
    rules    = models.ManyToManyField(Pattern, related_name='resources',blank=True)


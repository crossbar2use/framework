from django.contrib import admin

from .models import *

#*********************************************************************

class LanguageAdmin(admin.ModelAdmin):
    list_display  = ['name','code','flag']
    #list_filter  = ['frame','owner__email','speak__name']
    list_editable = ['code','flag']

admin.site.register(Language, LanguageAdmin)

######################################################################

class OrganismAdmin(admin.ModelAdmin):
    list_display  = ['name']
    #list_editable = ['name']

admin.site.register(Organism, OrganismAdmin)

#*********************************************************************

class BackendInline(admin.TabularInline):
    model = Backend

class IdentityAdmin(admin.ModelAdmin):
    list_display  = ['username','email','is_superuser','is_staff','first_name','last_name']
    list_filter   = ['is_superuser','is_staff']
    list_editable = ['is_superuser','is_staff','first_name','last_name']

    inlines       = [BackendInline]

admin.site.register(Identity, IdentityAdmin)

######################################################################

class BackendAdmin(admin.ModelAdmin):
    list_display  = ['owner','alias','proto','rlink']
    list_filter  = ['owner__email','proto']
    list_editable = ['proto','rlink']

admin.site.register(Backend, BackendAdmin)

######################################################################

class StoreInline(admin.TabularInline):
    model = BotStore

class TokenInline(admin.TabularInline):
    model = BotToken

class AgentAdmin(admin.ModelAdmin):
    list_display  = ['owner','alias','master_key','rest_token']
    #list_filter  = ['is_superuser','is_staff']
    list_editable = ['master_key','rest_token']

    inlines       = [TokenInline,StoreInline]

admin.site.register(BotAgent, AgentAdmin)

#*********************************************************************

class StoreAdmin(admin.ModelAdmin):
    list_display  = ['owner','proto','rlink']
    list_filter  = ['owner__pseudo','proto']
    list_editable = ['proto','rlink']
    #list_filter  = ['owner__email','proto']

admin.site.register(BotStore, StoreAdmin)

#*********************************************************************

class TokenAdmin(admin.ModelAdmin):
    list_display  = ['owner','place','value']
    list_filter  = ['owner__pseudo','place']
    list_editable = ['place','value']

admin.site.register(BotToken, TokenAdmin)


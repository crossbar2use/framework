import graphene

from graphene_django.types import DjangoObjectType
from graphene_django.filter import DjangoFilterConnectionField

from .models import *

#*********************************************************************

class LanguageType(DjangoObjectType):
    class Meta:
        model = Language
        filter_fields = ['name','code','flag']
        interfaces = (graphene.relay.Node, )

##########################################################################

class OrganismType(DjangoObjectType):
    class Meta:
        model = Organism
        filter_fields = ['name']
        interfaces = (graphene.relay.Node, )

#*********************************************************************

class IdentityType(DjangoObjectType):
    class Meta:
        model = Identity
        filter_fields = ['username','email','is_superuser','is_staff','first_name','last_name']
        interfaces = (graphene.relay.Node, )

##########################################################################

class AgentType(DjangoObjectType):
    class Meta:
        model = BotAgent
        filter_fields = ['owner','alias']
        interfaces = (graphene.relay.Node, )

##########################################################################

class Query(graphene.ObjectType):
    one_language = graphene.relay.Node.Field(LanguageType)
    all_language = DjangoFilterConnectionField(LanguageType)

    one_organism = graphene.relay.Node.Field(OrganismType)
    all_organism = DjangoFilterConnectionField(OrganismType)

    one_identity = graphene.relay.Node.Field(IdentityType)
    all_identity = DjangoFilterConnectionField(IdentityType)

    one_agent = graphene.relay.Node.Field(AgentType)
    all_agent = DjangoFilterConnectionField(AgentType)

#*********************************************************************

schema = graphene.Schema(query=Query)


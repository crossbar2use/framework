from django.conf.urls import include, url

from logics.basic import views

urlpatterns = [
    url(r'^login/session/', include('rest_social_auth.urls_session')),
    url(r'^login/token/', include('rest_social_auth.urls_token')),
    url(r'^login/jwt/', include('rest_social_auth.urls_jwt')),

    url(r'^logout/$', views.LogoutSessionView.as_view(), name='logout_session'),

    url(r'^user/session/', views.UserSessionDetailView.as_view(), name="current_user_session"),
    url(r'^user/token/', views.UserTokenDetailView.as_view(), name="current_user_token"),
    url(r'^user/jwt/', views.UserJWTDetailView.as_view(), name="current_user_jwt"),
]

from django.conf.urls import include, url

from logics.basic import views

urlpatterns = [
    url(r'^$', views.HomeSessionView.as_view(), name='home'),
    #url(r'^$', views.LandingView.as_view(), name='home'),

    url(r'^dashboard/session/$', views.HomeSessionView.as_view(), name='home_session'),
    url(r'^dashboard/token/$', views.HomeTokenView.as_view(), name='home_token'),
    url(r'^dashboard/jwt/$', views.HomeJWTView.as_view(), name='home_jwt'),

    url(r'^.*\.html', views.gentella_html, name='gentella'),
]

from django.contrib.auth.models import AbstractUser
from django.db import models

######################################################################

BACKEND_TYPEs = (
    ('cache', "Memcache daemon"),
    ('redis', "Redis or Sentinel"),

    ('sqldb', "MySQL or PostgreSQL"),
    ('nosql', "MongoDB database"),
    ('neo4j', "Neo4j instance"),

    ('parse', "Parse Server"),
    ('graph', "GraphQL engine"),

    ('queue', "AMQP instance"),
    ('topic', "MQTT instance"),
)

#*********************************************************************

class Language(models.Model):
    name      = models.CharField(max_length=64)
    code      = models.CharField(max_length=64, blank=True)
    flag      = models.CharField(max_length=64, blank=True)

    def __str__(self): return str(self.name)

######################################################################

class Organism(models.Model):
    name = models.CharField(max_length=64)
    link = models.URLField(null=True, blank=True)

    def __str__(self): return str(self.name)

#*********************************************************************

class Identity(AbstractUser):
    social_thumb = models.URLField(null=True, blank=True)

#*********************************************************************

class Backend(models.Model):
    owner   = models.ForeignKey(Identity, related_name='backends')
    alias   = models.CharField(max_length=64)

    proto   = models.CharField(max_length=24, choices=BACKEND_TYPEs)
    rlink   = models.CharField(max_length=512,blank=True)

    def __str__(self): return str(self.alias)

######################################################################

class BotAgent(models.Model):
    owner      = models.ForeignKey(Organism, related_name='agents')
    alias      = models.CharField(max_length=64)

    title      = models.CharField(max_length=256,blank=True)
    admin      = models.ForeignKey(Identity, related_name='agents',null=True,blank=True)

    pseudo     = models.CharField(max_length=64)
    passwd     = models.CharField(max_length=64)

    appli_uuid = models.CharField(max_length=256, blank=True)
    master_key = models.CharField(max_length=256, blank=True)
    rest_token = models.CharField(max_length=256, blank=True)

    heroku_key = models.CharField(max_length=256, blank=True)

    speak_lang = models.ManyToManyField(Language, blank=True)

    heroku_app = lambda self,*x: '%s-%s-%s.herokuapp.com' % (
        self.owner.alias,
        self.alias,
        '-'.join(x),
    )
    heroku_url = lambda self,*args,**kwargs: urljoin('http://%s',*args,**kwargs)

    def heroku_config(self, alias, context, **flags):
        context.update(dict(
            APP_ID     = self.appli_uuid,
            APP_NAME   = self.title,
            APP_SECRET = self.master_key,
            APP_TOKEN  = self.rest_token,
            APP_URL    = self.heroku_url(alias),
        ))

        if flags.get('disk',False):
            context.update(dict(
                HTTP_HOST='realm.uchikoma.online',
                HTTP_LINK='/parse/root',
                HTTP_PORT=80,

                FTP_HOST='ftp.cluster026.ovh.net',
                FTP_PASS='TestingIt',
                FTP_PATH='/uchikoma.online/realm/root',
                FTP_PORT=21,
                FTP_USER='tayaa-realm',
            ))

        if flags.get('mail',False):
            context.update(dict(
                SMTP_HOST='smtp.cluster026.ovh.net',
                SMTP_PASS='TestingIt',
                SMTP_PORT=485,
                SMTP_USER='realm@tayaa.me',
            ))

        for store in bot.backends.all():
            context['%s_URL' % store.frame.upper()] = store.rlink

        return context

    @property
    def heroku_deploy(self):
        yield dict(key='knows',env=self.heroku_config('knows',{
        }),ext=['heroku-postgres'])

        yield dict(key='memex',env=self.heroku_config('memex',{
'ADMIN_PSEUDO': self.pseudo,
'ADMIN_PASSWD': self.passwd,
        },disk=True,mail=True),ext=['heroku-postgres'])

        yield dict(key='nodes',env=self.heroku_config('nodes',{
'NODE_RED_USERNAME': self.pseudo,
'NODE_RED_PASSWORD': self.passwd,
        }),ext=['heroku-postgres'])

        yield dict(key='pipes',env=self.heroku_config('pipes',{
'AIRFLOW_USERNAME': self.pseudo,
'AIRFLOW_PASSWORD': self.passwd,
        }),ext=['heroku-postgres'])

        yield dict(key='speak',env=self.heroku_config('speak',{
'DATABASE': 'postgres',
'BOTPRESS_PASSWORD': self.passwd,
'MESSENGER_HOST': self.heroku_url('speak'),
        }),ext=['heroku-postgres'])

    @property
    def parse_servers(self):
        yield "PostgreSQL", 'http://%s/store' % self.heroku_app('memex')
        yield "MongoDB", 'http://%s/mongo' % self.heroku_app('memex')

    def __str__(self): return str(self.alias)

#*********************************************************************

class BotStore(models.Model):
    owner   = models.ForeignKey(BotAgent, related_name='backends')

    proto   = models.CharField(max_length=24, choices=BACKEND_TYPEs)
    rlink   = models.CharField(max_length=512,blank=True)

    def __str__(self): return str(self.alias)

######################################################################

TOKEN_TYPEs = (
    ('slack',    "Slack"),
    ('telegram', "Telegram"),

    ('github',   "GitHub"),
    ('bucket',   "BitBucket"),
    ('gitlab',   "GitLab"),
)

class BotToken(models.Model):
    owner    = models.ForeignKey(BotAgent, related_name='tokens')

    place    = models.CharField(max_length=24, choices=TOKEN_TYPEs)
    value    = models.CharField(max_length=256,null=True,blank=True)

    def __str__(self): return str(self.alias)

#*********************************************************************

SECRET_TYPEs = (
    ('google',   "Google oAuth2"),
    ('facebook', "Facebook oAuth2"),
    ('twitter',  "Twitter"),
    ('linkedin', "Linked In"),
)

class BotSecret(models.Model):
    owner    = models.ForeignKey(BotAgent, related_name='secrets')
    cloud    = models.CharField(max_length=24, choices=SECRET_TYPEs)

    appid    = models.CharField(max_length=128)
    token    = models.CharField(max_length=128)

    def __str__(self): return str(self.alias)


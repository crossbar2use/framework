from specs.shortcuts import *

from django.conf import settings
from django.db import connections
from django.utils.version import get_version

class Command(Commandline):
    """
    Runs RQ workers on specified queues. Note that all queues passed into a
    single rqworker command must share the same connection.

    Example usage:
    python manage.py rqworker high medium low
    """

    args = '<queue queue ...>'

    def add_arguments(self, parser):
        pass

    #************************************************************************************

    def handle(self, *args, **options):
        print "Configuring daemons :"

        for k,v in dict(
            #is_linked=True,
            is_linked=False,
            is_ontolog=False,

            has_clock=False,
            has_parse=False,
            has_queue=False,
        ).iteritems():
            setattr(self,k, v)

        print "\t-> Crossbar"

        self.write_json({
            "version": 2,
            "workers": [
                {
                    "type": "router",
                    "options": {
                        "pythonpath": [
                            "."
                        ]
                    },
                    "realms": [self.read_json('works','namespace.json')],
                    "transports": [
                        {
                            "type": "web",
                            "endpoint": {
                                "type": "tcp",
                                "port": "$PORT"
                            },
                            "paths": dict([(k,v) for k,v in self.wamp_routing]),
                        }
                    ],
                    "components": self.wamp_applet,
                }
            ]+[cfg for cfg in self.wamp_worker]
        }
        ,'config.json')

        if self.has_parse:
            print "\t-> Parse Dashboard"

            self.write_json({
                "apps": [cfg in self.parse_servers],
                "iconsFolder": "icons",
                "users": [{
                    "user":"user1","pass":"pass"
                },{
                    "user":"user2","pass":"pass"
                }],
                "useEncryptedPasswords": False
            },'dashlet.json')

        if self.is_linked:
            print "\t-> Linked Data Fragments"

            self.write_json({
                "title": "NeuroChip :: Linked Data (fragments)",
                "protocol": "http",
                "baseURL": raddr('fragm'),
                "datasources": dict([(k,v) for k,v in self.triple_fragments]),
            },'linked.json')

        print "Starting the daemon :"

        if 'PORT' not in os.environ:
            os.environ['PORT'] = str(settings.MAIN_PORT)

        os.environ['PATH'] = 'node_modules/.bin:%(PATH)s' % os.environ

        self.shell('crossbar','start','--cbdir=.')

    #####################################################################################

    @property
    def parse_servers(self):
        for bot in BotAgents.objects.all():
            for key,url in bot.parse_servers:
                yield {
                    "serverURL": url,
                    "appId": bot.appli_uuid,
                    "masterKey": bot.master_key,
                    "appName": "Bot :: %s (%s)" % (bot.alias,key),
                    #"primaryBackgroundColor": "#FFA500",
                    #"secondaryBackgroundColor": "#FF4500",
                    #"iconName": "MyAppIcon.png",
                    "supportedPushLocales": [lang.code for lang in bot.speak_lang.all()]
                }

    #************************************************************************************

    @property
    def triple_fragments(self):
        for item in Vocabular.objects.filter(frame='hdt',shows=True):
            yield item.alias,{
              "title": item.title,
              "type": "HdtDatasource",
              "description": item.helps,
              "settings": {
                "file": "data/dbpedia2014.hdt",
              }
            }

        for item in GraphEngine.objects.filter(frame='rdf',shows=True):
            yield item.alias,{
              "title": item.title,
              "type": "SparqlDatasource",
              "description": item.helps,
              "settings": {
                "endpoint": item.rlink,
                "defaultGraph": item.graph,
              }
            }

    #####################################################################################

    @property
    def wamp_applet(self):
        middleware = []

        for path,role in settings.WAMP_APPLET.iteritems():
            middleware.append({
                "type": "class",
                "classname": path,
                "realm": settings.WAMP_NSPACE,
                "role": role,
            })

        return middleware

    #************************************************************************************

    @property
    def wamp_routing(self):
        yield '/',{
            "type": "wsgi",
            "module": "specs.wsgi",
            "object": "application"
        }

        if self.has_parse:
            yield 'parse',{
                "type": "reverseproxy",
                "host": "localhost",
                "port": self.port('parse'),
                "path": "/",
            }

        yield 'media',{
            "type": "static",
            "directory": "media"
        }

        yield 'static',{
            "type": "static",
            "directory": "works/static"
        }

        if self.is_linked:
            yield 'fragm',{
                "type": "reverseproxy",
                "host": "localhost",
                "port": self.port('fragm'),
                #"path": "/",
            }

        yield 'spine',{
            "type": "websocket",
            "serializers": [
                "json"
            ],
            "auth": {
                "ticket": {
                    "type": "dynamic",
                    "authenticator": "neuro.chip.authentify"
                }
            }
        }

        if self.is_ontolog:
            yield 'hylar',{
                "type": "reverseproxy",
                "host": "localhost",
                "port": self.port('hylar'),
                #"path": "/",
            }

    #************************************************************************************

    @property
    def wamp_worker(self):
        if False: #self.has_hygql:
            yield {
                "type": "guest",
                "executable": "java",
                "arguments": ["-jar","build/libs/hypergraphql-1.0.3-exe.jar","--config","/hgql/config/config.json"],
                "options": { "workdir": ".", "env": { "inherit": True, "vars": {

                }}}
            }

        if self.has_clock:
            yield {
                "type": "guest",
                "executable": "./manage.py",
                "arguments": ["rqscheduler", "-i", str(self.has_clock)],
                "options": { "workdir": ".", "env": { "inherit": True, "vars": {

                }}}
            }

        if self.has_queue:
            yield {
                "type": "guest",
                "executable": "./manage.py",
                "arguments": ["rqworker", "-i", "30"],
                "options": { "workdir": ".", "env": { "inherit": True, "vars": {

                }}}
            }

        if self.has_parse:
            yield {
                "executable": "parse-dashboard",
                "type": "guest",
                "arguments": ["--config","dashlet.json"],
                "options": { "workdir": ".", "env": { "inherit": True, "vars": {

                }}}
            }

        if self.is_linked:
            yield {
                "executable": "ldf-server",
                "type": "guest",
                "arguments": ["linked.json", str(self.port('fragm')), "2"],
                "options": { "workdir": ".", "env": { "inherit": True, "vars": {

                }}}
            }

        if self.is_ontolog:
            yield {
                "executable": "hylar",
                "type": "guest",
                "arguments": ["--port",str(self.port('hylar')),"-od","/usr/local/share/ontologies/"],
                "options": { "workdir": ".", "env": { "inherit": True, "vars": {

                }}}
            }

import graphene

from graphene_django.types import DjangoObjectType
from graphene_django.filter import DjangoFilterConnectionField

from .models import *

##########################################################################

class PhoneType(DjangoObjectType):
    class Meta:
        model = Phone
        filter_fields = ['owner','alias','hw_addr','ip4addr','ip6addr']
        interfaces = (graphene.relay.Node, )

#*********************************************************************

class MachineType(DjangoObjectType):
    class Meta:
        model = Machine
        filter_fields = ['owner','alias','hw_addr','ip4addr','ip6addr']
        interfaces = (graphene.relay.Node, )

#*********************************************************************

class RouterType(DjangoObjectType):
    class Meta:
        model = Router
        filter_fields = ['owner','alias','hw_addr','ip4addr','ip6addr']
        interfaces = (graphene.relay.Node, )

##########################################################################

class Query(graphene.ObjectType):
    one_phone = graphene.relay.Node.Field(MachineType)
    all_phone = DjangoFilterConnectionField(MachineType)

    one_machine = graphene.relay.Node.Field(MachineType)
    all_machine = DjangoFilterConnectionField(MachineType)

    one_router = graphene.relay.Node.Field(RouterType)
    all_router = DjangoFilterConnectionField(RouterType)

#*********************************************************************

schema = graphene.Schema(query=Query)


from django.contrib import admin

from .models import *

######################################################################

class MachineAdmin(admin.ModelAdmin):
    list_display  = ['owner','alias','hw_addr','ip4addr','ip6addr']
    list_filter  = ['owner__name']
    list_editable = ['alias','ip4addr','ip6addr']

admin.site.register(Machine, MachineAdmin)

#*********************************************************************

class PhoneAdmin(admin.ModelAdmin):
    list_display  = ['owner','alias','hw_addr','ip4addr','ip6addr']
    list_filter  = ['owner__email']
    list_editable = ['alias','ip4addr','ip6addr']

admin.site.register(Phone, PhoneAdmin)

#*********************************************************************

class RouterAdmin(admin.ModelAdmin):
    list_display  = ['owner','alias','hw_addr','ip4addr','ip6addr']
    list_filter  = ['owner__name']
    list_editable = ['alias','ip4addr','ip6addr']

admin.site.register(Router, RouterAdmin)

######################################################################



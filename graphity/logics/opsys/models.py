from django.contrib.auth.models import AbstractUser
from django.db import models

from logics.basic.models import *

#################################################################################

class Machine(models.Model):
    owner    = models.ForeignKey(Organism, related_name='machines')
    alias    = models.CharField(max_length=64)

    hw_addr  = models.CharField(max_length=256, blank=True)
    ip4addr  = models.CharField(max_length=256, blank=True)
    ip6addr  = models.CharField(max_length=256, blank=True)

    username = models.CharField(max_length=256, default='root')
    password = models.CharField(max_length=256, blank=True)

    def __str__(self): return str(self.alias)

#*********************************************************************************

class Phone(models.Model):
    owner    = models.ForeignKey(Identity, related_name='phones')
    alias    = models.CharField(max_length=64)

    hw_addr  = models.CharField(max_length=256, blank=True)
    ip4addr  = models.CharField(max_length=256, blank=True)
    ip6addr  = models.CharField(max_length=256, blank=True)

    username = models.CharField(max_length=256, default='root')
    password = models.CharField(max_length=256, blank=True)

    def __str__(self): return str(self.alias)

#*********************************************************************************

class Router(models.Model):
    owner    = models.ForeignKey(Organism, related_name='routers')
    alias    = models.CharField(max_length=64)

    hw_addr  = models.CharField(max_length=256, blank=True)
    ip4addr  = models.CharField(max_length=256, blank=True)
    ip6addr  = models.CharField(max_length=256, blank=True)

    username = models.CharField(max_length=256, default='root')
    password = models.CharField(max_length=256, blank=True)

    def __str__(self): return str(self.alias)

#################################################################################



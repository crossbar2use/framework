import requests
import simplejson as json, yaml
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render
from django.views.generic.base import View, TemplateView

class ProxyQL(View):
    def get(self, request, *args, **kwargs):
        context = {}

        for key in ('query','variables','operationName'):
            context[key] = request.GET.get(key)

        from logics.daten.models import GraphQueryEngine

        try:
            engine = GraphQueryEngine.objects.get(alias=self.kwargs['alias'])
        except:
            raise Http404()

        context['endpoint'] = "ql"

        return render(request, 'graphene/proxyql.html', context)

    def post(self, context, *args, **kwargs):
        payload = json.loads(context.read().decode('utf-8'))

        from logics.daten.models import GraphQueryEngine

        try:
            engine = GraphQueryEngine.objects.get(alias=self.kwargs['alias'])
        except:
            raise Http404()

        req = requests.post(engine.rlink,
            #data=request.GET,
            headers={
                'Content-Type': "application/json",
                'Accept': "application/json"
            },
            json=payload,
        timeout=1.7, stream=False)

        #req.raise_for_status()

        res = req.json()

        return JsonResponse(res, safe=True)


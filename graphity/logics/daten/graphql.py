import graphene

from graphene_django.types import DjangoObjectType
from graphene_django.filter import DjangoFilterConnectionField

from .models import *

##########################################################################

class BackendType(DjangoObjectType):
    class Meta:
        model = Backend
        filter_fields = ['owner','alias','proto','rlink']
        interfaces = (graphene.relay.Node, )

##########################################################################

class DeclaredType(DjangoObjectType):
    class Meta:
        model = CustomType
        filter_fields = ['name','text','std_py2','std_py3','std_csv','std_rdf','std_parse','std_wpacf']
        interfaces = (graphene.relay.Node, )

#*********************************************************************

class SchemaType(DjangoObjectType):
    class Meta:
        model = CustomSchema
        filter_fields = ['owner','alias']
        interfaces = (graphene.relay.Node, )

#*********************************************************************

class FieldType(DjangoObjectType):
    class Meta:
        model = CustomField
        filter_fields = ['model','alias','vtype','empty']
        interfaces = (graphene.relay.Node, )

##########################################################################

class GraphEngineType(DjangoObjectType):
    class Meta:
        model = GraphEngine
        filter_fields = ['owner','alias','categ','rlink']
        interfaces = (graphene.relay.Node, )

#*********************************************************************

class GraphQueryType(DjangoObjectType):
    class Meta:
        model = GraphQuery
        filter_fields = ['owner','alias','engin','query']
        interfaces = (graphene.relay.Node, )

##########################################################################

class Query(graphene.ObjectType):
    one_backend = graphene.relay.Node.Field(BackendType)
    all_backend = DjangoFilterConnectionField(BackendType)

    all_type = DjangoFilterConnectionField(DeclaredType)
    one_type = graphene.relay.Node.Field(DeclaredType)

    all_schema = DjangoFilterConnectionField(SchemaType)
    one_schema = graphene.relay.Node.Field(SchemaType)

    all_field = DjangoFilterConnectionField(FieldType)
    one_field = graphene.relay.Node.Field(SchemaType)

    one_graph_engine = graphene.relay.Node.Field(GraphEngineType)
    all_graph_engine = DjangoFilterConnectionField(GraphEngineType)

    one_graph_query = graphene.relay.Node.Field(GraphQueryType)
    all_graph_query = DjangoFilterConnectionField(GraphQueryType)

#*********************************************************************

schema = graphene.Schema(query=Query)


from django.conf.urls import include, url

from graphene_django.views import GraphQLView
from specs.graphql import mapped

from logics.daten import views

urlpatterns = [
    url(r'^ql', GraphQLView.as_view(schema=mapped[None], graphiql=False)),
    url(r'^iql', GraphQLView.as_view(schema=mapped[None], graphiql=True)),

    url(r'^-(?P<alias>\w+)/ql', views.ProxyQL.as_view()),
] + [
    url(r'^~'+nrw+'/ql', GraphQLView.as_view(
        schema=mapped[nrw],
        graphiql=True,
    ))
    for nrw in mapped
    if nrw is not None
]

from django.conf.urls import include, url

from graphene_django.views import GraphQLView
from specs.graphql import mapped

from logics.daten import views

urlpatterns = [
    url(r'^gql/proxy/(?P<alias>\w+)', views.ProxyQL.as_view()),
] + [
    url(r'^gql/logic/'+nrw, GraphQLView.as_view(
        schema=mapped[nrw],
        graphiql=True,
    ))
    for nrw in mapped
    if nrw is not None
]

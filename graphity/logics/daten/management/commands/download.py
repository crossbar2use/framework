from specs.shortcuts import *

from django.conf import settings
from django.db import connections
from django.utils.version import get_version

class Command(Commandline):
    """
    Example usage:
    python manage.py rqworker high medium low
    """

    args = '<queue queue ...>'

    def add_arguments(self, parser):
        parser.add_argument('--worker-class', action='store', dest='worker_class',
                            help='RQ Worker class to use')
        parser.add_argument('--pid', action='store', dest='pid',
                            default=None, help='PID file to write the worker`s pid into')
        parser.add_argument('--burst', action='store_true', dest='burst',
                            default=False, help='Run worker in burst mode')
        parser.add_argument('--name', action='store', dest='name',
                            default=None, help='Name of the worker')
        parser.add_argument('--queue-class', action='store', dest='queue_class',
                            help='Queues class to use')
        parser.add_argument('--job-class', action='store', dest='job_class',
                            help='Jobs class to use')
        parser.add_argument('--worker-ttl', action='store', type=int,
                            dest='worker_ttl', default=420,
                            help='Default worker timeout to be used')
        parser.add_argument('--sentry-dsn', action='store', default=None, dest='sentry-dsn',
                            help='Report exceptions to this Sentry DSN')

    #****************************************************************************************

    def handle(self, *args, **options):
        for handle,workdir,title in tqdm([
(self.nlp_tool,['nlp','nltk'],"NLTK corporas"),
(self.nlp_core,['nlp','core'],"CoreNLP corporas"),

(self.rdf_owl,['owl'],"OWL ontologies"),
(self.rdf_xml,['rdf'],"RDF-XML files"),
(self.rdf_ttl,['rdf'],"Turtles files"),
(self.rdf_hdt,['hdt'],"HDT datasets"),

#(self.cv2_zoo,[''],"OpenCV"),

(self.eye_cv2,['eye','cv2','model-zoo'],"OpenCV Model Zoo"),
#(self.eye_ocr,['eye','ocr','tesseract'],"Tesseract OCR data"),
        ]):
            print "\t%s :" % title

            os.chdir(rpath('daten',*workdir))

            handle(**options)

    #########################################################################################

    def nlp_tool(self, **options):
        self.shell('python','-m','nltk.downloader','all')

    #****************************************************************************************

    def eye_cv2(self, **options):
        self.shell('./downloader.py','--all')

    def eye_ocr(self, **options):
        self.download(
'downloader',
        )


from django.contrib.auth.models import AbstractUser
from django.db import models

from logics.neuro.models import *

######################################################################

CARDINALITYs = (
    ('*',"None or Many"),
    ('?',"None or One"),
    ('+',"One at least"),
    ('1',"Exactly One"),
)

class CustomType(models.Model):
    name      = models.CharField(max_length=64)
    text      = models.CharField(max_length=64,blank=True)
    numb      = models.CharField(max_length=64,default='1',choices=CARDINALITYs)
    base      = models.ForeignKey("CustomType", related_name='nested', blank=True, null=True, default=None)

    std_py2   = models.CharField(max_length=64,blank=True)
    std_py3   = models.CharField(max_length=64,blank=True)

    std_csv   = models.CharField(max_length=64,blank=True)
    std_gql   = models.CharField(max_length=64,blank=True)
    std_rdf   = models.CharField(max_length=64,blank=True)

    std_parse = models.CharField(max_length=64,blank=True)
    std_wpacf = models.CharField(max_length=64,blank=True)

    def __str__(self): return str(self.text or self.name)

#*********************************************************************

class CustomSchema(models.Model):
    owner   = models.ForeignKey(Identity, related_name='schemas')
    alias   = models.CharField(max_length=32)

    rdf_type = models.CharField(max_length=128, blank=True)
    rdf_name = models.CharField(max_length=128, blank=True)

    owl_link = models.CharField(max_length=256, blank=True)

    json_ld  = models.TextField(blank=True)

    def normalize(self, payload):
        entry = payload

        return entry

    def __str__(self): return str(self.alias)

#*********************************************************************

class CustomField(models.Model):
    model    = models.ForeignKey(CustomSchema, related_name='fields')
    alias    = models.CharField(max_length=64)

    vtype    = models.ForeignKey(CustomType, related_name='fields',null=True,blank=True)
    empty    = models.CharField(max_length=64,blank=True)

    owl_rule = models.CharField(max_length=256,blank=True)
    xsd_attr = models.CharField(max_length=256,blank=True)
    rdf_type = models.CharField(max_length=256,blank=True)

    json_ld  = models.TextField(blank=True)

    def __str__(self): return str(self.alias)

    def validate(self, value):
        return True

    def extracts(self, record):
        return getattr(record, self.alias, self.default)

#*********************************************************************

class CustomEntity(models.Model):
    model   = models.ForeignKey(CustomSchema, related_name='entities')
    alias   = models.CharField(max_length=64)

    narrow  = models.CharField(max_length=64)
    origin  = models.URLField()

    hashed  = property(lambda self: md5(self.origin))
    cached  = property(lambda self: rpath('works','cacher',self.hashed))

    relpath = lambda self,*x : rpath('mapping',self._prn.alias,self.hashed,*x)

    #***************************************************************************

    def save(self, *args, **kwargs):
        if len(self.narrow.strip())==0:
            self.narrow = self.hashed

        return super(self,CustomEntity).save(*args, **kwargs)

    @property
    def payload(self):
        if not hasattr(self, '_data'):
            entry = {}

            setattr(self, '_data', entry)

        return self.model.normalize(entry)

    #***************************************************************************

    def __json__(self):
        return dict([(f.alias,f.extracts(self.payload)) for f in self.model.fields.all()])

    def __jsonld__(self):
        doc = {}

        for f in self.model.fields.all():
            if f.alias in self._prn.JSON_LD:
                cfg = self._prn.JSON_LD[f.alias]

                if type(cfg) is dict:
                    doc[cfg['@id']] = {
                        cfg['@type'] : f.extracts(self.payload),
                    }
                else:
                    doc[cfg] = f.extracts(self)

        return jsonld.compact(doc, self._prn.JSON_LD)

    #***************************************************************************

    def __rdflib__(self):
        res = BNode()

        if self._nrw is not None:
            res = URIRef(self._prn.RDF_LINK % self._nrw)

        yield res, RdfNS.RDF.Type, self._prn.RDF_TYPE

        for f in self.model.fields.all():
            if f.rdf_ns is not None:
                yield res, f.rdf_ns, Literal(f.extracts(self.payload))

    def __owllib__(self):
        onto = None

        return onto

######################################################################

GRAPH_BACKENDs = (
    ('rdf', "RDF/SPARQL store"),
    ('gql', "GraphQL endpoint"),
    ('neo', "Neo4j database"),
)

class GraphEngine(models.Model):
    owner   = models.ForeignKey(Identity, related_name='graph_endpoints')
    alias   = models.CharField(max_length=64)
    frame    = models.CharField(max_length=24, choices=GRAPH_BACKENDs)

    title   = models.CharField(max_length=128, blank=True)
    helps   = models.TextField(blank=True)
    categ   = models.CharField(max_length=64, blank=True)

    shows   = models.BooleanField(default=False)
    rlink   = models.URLField()
    cover   = models.URLField(blank=True)

    def __str__(self): return str(self.alias)

#*********************************************************************

class GraphQuery(models.Model):
    owner   = models.ForeignKey(Identity, related_name='graph_queries')
    alias   = models.CharField(max_length=64)

    engin   = models.ForeignKey(GraphEngine, related_name='queries')
    query   = models.TextField(blank=True)

    def __str__(self): return str(self.alias)

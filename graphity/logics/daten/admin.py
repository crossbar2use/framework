from django.contrib import admin

from .models import *

######################################################################

class TypeAdmin(admin.ModelAdmin):
    # ['name','text','std_py2','std_py3','std_csv','std_rdf','std_parse','std_wpacf']
    list_display  = ['name','text','std_rdf','std_parse','std_wpacf']
    #list_filter  = ['owner__email']
    list_editable = ['text','std_rdf','std_parse','std_wpacf']

admin.site.register(CustomType, TypeAdmin)

#*********************************************************************

class SchemaInline(admin.TabularInline):
    model = CustomField

class SchemaAdmin(admin.ModelAdmin):
    list_display  = ['owner','alias']
    list_filter  = ['owner__email']
    list_editable = ['alias']

    inlines = [SchemaInline]

admin.site.register(CustomSchema, SchemaAdmin)

#*********************************************************************

class FieldAdmin(admin.ModelAdmin):
    list_display  = ['model','alias','vtype','empty']
    list_filter  = ['model__alias']
    list_editable = ['alias','vtype','empty']

admin.site.register(CustomField, FieldAdmin)

#*********************************************************************

class BridgeAdmin(admin.ModelAdmin):
    pass

#admin.site.register(CustomBridge, BridgeAdmin)

######################################################################

class GraphEngineAdmin(admin.ModelAdmin):
    list_display  = ['owner','alias','shows','rlink','categ']
    list_filter  = ['owner__email','categ','shows']
    list_editable = ['rlink','shows','categ']

admin.site.register(GraphEngine, GraphEngineAdmin)

#*********************************************************************

class GraphQueryAdmin(admin.ModelAdmin):
    list_display  = ['owner','alias','engin','query']
    list_filter  = ['owner__email','engin']
    list_editable = ['engin','query']

admin.site.register(GraphQuery, GraphQueryAdmin)


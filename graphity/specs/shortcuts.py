from .utils import *

from django.core.management.base import BaseCommand

class Commandline(BaseCommand,DatenMixin,PosixMixin,WiresMixin):
    pass

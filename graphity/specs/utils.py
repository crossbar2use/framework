from .helpers import *

#**********************************************************************

class BaseClass(object):
    def __init__(self, *args, **kwargs):
        self.trigger('initialize', *args, **kwargs)

    def trigger(self, method, *args, **kwargs):
        handler = getattr(self, method, None)

        if callable(handler):
            return handler(*args, **kwargs)

        return None

        raise NotImplemented()

#**********************************************************************

class Feature(BaseClass):
    pass

#######################################################################

class DatenMixin:
    def read_yaml(self, *target):
        return yaml.load(self.open_file(*target))

    def write_yaml(self, payload, *target):
        return self.write_file(yaml.dumps(payload), *target)

    def read_json(self, *target):
        return json.load(self.open_file(*target))

    def write_json(self, payload, *target):
        return self.write_file(json.dumps(payload), *target)

#**********************************************************************

class PosixMixin:
    def open_file(self, *target):
        return open(rpath(*target))

    def read_file(self, *target):
        return self.open_file(*target).read()

    def write_file(self, payload, *target):
        with open(rpath(*target),'w+') as f:
            f.write(payload)
        return payload

    def shell(self, program, *arguments):
        stmt = []

        for part in arguments:
            if ' ' in part:
                part = '"%s"' % part

            stmt.append(part)

        return os.system(' '.join([program]+stmt))

#**********************************************************************

class WiresMixin:
    def port(self, alias, value=None):
        if not hasattr(self,'ports'):
            setattr(self,'ports',{})

        if alias in self.ports:
            return self.ports[alias]
        else:
            if value is None:
                with closing(socket.socket(socket.AF_INET, socket.SOCK_STREAM)) as s:
                    s.bind(('', 0))

                    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

                    value = s.getsockname()[1]

            self.ports[alias] = value

            return self.ports[alias]

    def download(self, *links):
        self.shell('wget','-c',*links)

#######################################################################

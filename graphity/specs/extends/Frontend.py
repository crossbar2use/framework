def conf_general(request):
    resp = dict(
        app={
            'name': "Gentelella Alela!",
            'link': "/",
        },
    )

    return resp

def user_avatar(request):
    resp = dict(
        avatars={
            'john': {
                'title': "John Doe",
                'cover': '/static/images/img.jpg',
            },
        },
    )

    for alias in resp['avatars']:
        resp['avatars'][alias]['alias'] = alias

    resp['profile'] = resp['avatars']['john']

    return resp

from logics.asset.models import WebSite
from logics.basic.models import BACKEND_TYPEs, Backend, BotStore
from logics.daten.models import GraphEngine

def suit_menus(request):
    resp = dict(
        catalog={
            'website': WebSite.objects.filter(is_gql=True),

            'graphql': GraphEngine.objects.filter(frame='rdf',shows=True),
            'triples': GraphEngine.objects.filter(frame='gql',shows=True),
        },
        backend={
            'daten': {},
            'store': {},
        },
    )

    for key in dict(BACKEND_TYPEs).keys():
        resp['backend']['daten'][key] = Backend.objects.filter(proto=key)
        resp['backend']['store'][key] = BotStore.objects.filter(proto=key)

    return resp


import graphene

import logics.asset.graphql as asset_ql
import logics.basic.graphql as basic_ql
import logics.crawl.graphql as crawl_ql
import logics.daten.graphql as daten_ql
import logics.opsys.graphql as opsys_ql
import logics.neuro.graphql as neuro_ql

class Query(
    asset_ql.Query,
    basic_ql.Query,
    #crawl_ql.Query,
    daten_ql.Query,
    opsys_ql.Query,
    neuro_ql.Query,
graphene.ObjectType):
    # This class will inherit from multiple Queries
    # as we begin to add more apps to our project
    pass

schema = graphene.Schema(query=Query)

mapped = {
    None:    schema,
    'asset': asset_ql.schema,
    'basic': basic_ql.schema,
    #'crawl': crawl_ql.schema,
    'daten': daten_ql.schema,
    'opsys': opsys_ql.schema,
    'neuro': neuro_ql.schema,
}


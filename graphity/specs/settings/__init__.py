from specs.helpers import *

DEBUG = True

MAIN_PORT = decoupled('PORT',cast=int,default=8000)
MAIN_LINK = decoupled('APP_URL',default='http://localhost:%d' % MAIN_PORT)

ROOT_URLCONF = 'specs.routing'

GRAPHENE = {
    'SCHEMA': 'specs.graphql.schema',
    'SCHEMA_OUTPUT': rpath('works','model','schema.graphql')
}

WSGI_APPLICATION = 'specs.wsgi.application'

ALLOWED_HOSTS = ['*']

AUTH_USER_MODEL = 'basic.Identity'

from specs.settings.Locale import *
from specs.settings.Stores import *
from specs.settings.Access import *
from specs.settings.Viewer import *
from specs.settings.Extend import *

INSTALLED_APPS = (
    'suit',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'suit_rq',

    'rest_framework',
    'rest_framework.authtoken',
    'social_django',
    'rest_social_auth',
    'rest_framework_swagger',
    'django_rq',
    'scheduler',
    'wooey',
    'graphene_django',
    #'mptt',
    #'elfinder',

    'logics.basic',
    'logics.daten',
    'logics.opsys',

    'logics.neuro',
    'logics.asset',

    'logics.crawl',
)

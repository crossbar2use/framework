from specs.helpers import *

SUIT_CONFIG = {
    # header
    'ADMIN_NAME': decoupled('APP_NAME'),
    'HEADER_DATE_FORMAT': 'l, j. F Y',
    'HEADER_TIME_FORMAT': 'H:i',

    # forms
    'SHOW_REQUIRED_ASTERISK': True,  # Default True
    'CONFIRM_UNSAVED_CHANGES': True, # Default True

    # menu
    'SEARCH_URL': '/admin/auth/user/',
    'MENU_ICONS': {
       'sites': 'icon-leaf',
       'auth': 'icon-lock',
    },
    'MENU_OPEN_FIRST_CHILD': False, # Default True
    #'MENU_EXCLUDE': (
    #    'auth.group',
    #),
    'MENU': (
        {'label': 'Access', 'icon':'icon-lock', 'models': (
            'authtoken.token','speak.bottoken',
            'social_django.association','social_django.nonce','social_django.usersocialauth',
        )},
        {'label': 'Formal', 'icon':'icon-user', 'models': (
            'basic.identity','speak.botagent',
            'auth.group','basic.organism',
        )},
        {'label': 'Entity', 'icon':'icon-briefcase', 'models': (
            'speak.botlexer','speak.botvocab',
        )},
        {'label': 'Linked', 'icon':'icon-globe', 'models': (
            'daten.backend','daten.endpoint',
        )},
        {'label': 'Device', 'icon':'icon-hdd', 'models': (
            'opsys.machine','opsys.phone','opsys.router',
        )},
        {'label': 'SysAdm', 'icon':'icon-signal', 'models': (
            'basic.backend','opsys.website',
            'scheduler.repeatablejob','scheduler.scheduledjob',
        )},
        {'label': 'Runner', 'app': 'wooey', 'icon':'icon-tasks'},
        {'label': 'Queues', 'icon':'icon-cog', 'url': '/clock/'},
    ),
    #'MENU': (
    #    'sites',
    #    {'app': 'auth', 'icon':'icon-lock', 'models': ('user', 'group')},
    #    {'label': 'Settings', 'icon':'icon-cog', 'models': ('auth.user', 'auth.group')},
    #    {'label': 'Support', 'icon':'icon-question-sign', 'url': '/support/'},
    #),
    'LIST_PER_PAGE': 50
}

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(BASE_DIR, 'theme'),
            # insert your TEMPLATE_DIRS here
            os.path.join(BASE_DIR, 'views'),
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                # Insert your TEMPLATE_CONTEXT_PROCESSORS here or use this
                # list if you haven't customized them:
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.request',
                'django.template.context_processors.request',

                'specs.extends.Frontend.conf_general',
                'specs.extends.Frontend.user_avatar',
                'specs.extends.Frontend.suit_menus',
            ],
        },
    },
]

# DRF settings

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '%(levelname)s:%(name)s: %(message)s '
                      '(%(asctime)s; %(filename)s:%(lineno)d)',
            'datefmt': "%Y-%m-%d %H:%M:%S",
        },
    },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose',
        },
    },
    'loggers': {
        'rest_social_auth': {
            'handlers': ['console', ],
            'level': "DEBUG",
        },
    }
}


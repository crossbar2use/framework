from specs.helpers import *

# Database
# https://docs.djangoproject.com/en/1.8/ref/settings/#databases
DATABASES = {
    'local': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'works','local.sqlite'),
    },
}

if 'DATABASE_URL' in os.environ:
    DATABASES['cloud'] = dj_database_url.config(conn_max_age=600, ssl_require=True)

if not os.path.exists(DATABASES['local']['NAME']):
    os.system('touch %(NAME)s' % DATABASES['local'])

DATABASES['default'] = DATABASES.get('cloud', DATABASES['local'])

RQ_QUEUES = {
    'default': {
        'URL': decoupled('REDIS_URL', 'redis://localhost:6379/0'), # If you're on Heroku
        'DEFAULT_TIMEOUT': 360,
    },
}

#RQ_EXCEPTION_HANDLERS = ['path.to.my.handler'] # If you need custom exception handlers


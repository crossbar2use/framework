from specs.helpers import *

# valid redirect domain for all apps: http://restsocialexample.com:8000/

SOCIAL_AUTH_FACEBOOK_APP = decoupled('OAUTH_FACEBOOK_APP',default='342150670028793')
SOCIAL_AUTH_FACEBOOK_SECRET = decoupled('OAUTH_FACEBOOK_APP',default='011aa729dd01856d533e1186a2a5737d')
SOCIAL_AUTH_FACEBOOK_SCOPE = decoupled('OAUTH_FACEBOOK_ACL',cast=lambda v: [s.strip() for s in v.split(',')],default='')
SOCIAL_AUTH_FACEBOOK_PROFILE_EXTRA_PARAMS = {
    'fields': ','.join([
        # public_profile
        'id', 'cover', 'name', 'first_name', 'last_name', 'age_range', 'link',
        'gender', 'locale', 'picture', 'timezone', 'updated_time', 'verified',
        # extra fields
        'email',
    ]),
}

SOCIAL_AUTH_GOOGLE_OAUTH2_APP = decoupled('OAUTH_GOOGLE_APP',default='718701035833-se553dglqla9rtrqb8e3spp522sknerh.apps.googleusercontent.com')
SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET = decoupled('OAUTH_GOOGLE_KEY',default='1oelIYSzlNl8-oF6302P3eFN')
SOCIAL_AUTH_GOOGLE_OAUTH2_SCOPE = decoupled('OAUTH_GOOGLE_ACL',cast=lambda v: [s.strip() for s in v.split(',')],default='')

SOCIAL_AUTH_TWITTER_APP = decoupled('OAUTH_TWITTER_APP',default='')
SOCIAL_AUTH_TWITTER_SECRET = decoupled('OAUTH_TWITTER_KEY',default='')

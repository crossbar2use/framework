import re,django
from django.conf.urls import include, url
from django.contrib import admin

from rest_framework_swagger.views import get_swagger_view

from graphene_django.views import GraphQLView
from specs.graphql import mapped

urlpatterns = [
    url(r'^',        include('logics.basic.routing.Front')),
    url(r'^oauth/',  include('logics.basic.routing.oAuth')),

    url(r'^admin/',  include(admin.site.urls)),
    url(r'^swagg/$', get_swagger_view(title='Pastebin API')),


    #url(r'^asset/',  include('logics.asset.urls')),
    #url(r'^crawl/',  include('logics.crawl.urls')),
    url(r'^query/',  include('logics.daten.routing.query')),
    url(r'^graph/',  include('logics.daten.routing.graph')),
    #url(r'^opsys/',  include('logics.opsys.urls')),

    #url(r'^brain/',  include('logics.neuro.routing.brain')),
    #url(r'^sight/',  include('logics.neuro.routing.sight')),
    #url(r'^tongs/',  include('logics.neuro.routing.tongs')),

    url(r'^engin/',  include('wooey.urls')),
    url(r'^clock/',  include('django_rq.urls')),
    #url(r'^drive/', include('elfinder.urls')),
]


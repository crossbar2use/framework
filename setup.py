
from setuptools import setup, find_packages

setup(
    name='crossbar2use',
    version='0.4',
    packages=find_packages(exclude=['tests*']),
    license='MIT',
    description='An example python package',
    long_description=open('README.txt').read(),
    install_requires=['cffi','crossbar'],
    url='https://github.com/crossbar2use/framework',
    author='TAYAA Med Amine',
    author_email='tayamino@gmail.com'
)

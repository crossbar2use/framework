#!/usr/bin/env python

import os,sys,click

from urllib.parse import urlparse

#*******************************************************************************

def shell(cmd, *args):
    stmt = [cmd]

    for x in args:
        x = str(x)

        if " " in x:
            x = '"'+x+'"'

        stmt.append(x)

    os.system(' '.join(stmt))

################################################################################

@click.group()
#@click.option('--count', default=1, help='Number of greetings.')
#@click.option('--name', prompt='Your name', help='The person to greet.')
@click.option('--debug/--no-debug', default=False)
def cli(debug):
    click.echo('Debug mode is %s' % ('on' if debug else 'off'))

#*******************************************************************************

@cli.command('help')
def documentation():
    """Show help message for the commandline."""
    for x in range(count):
        click.echo('Hello %s!' % name)

################################################################################

@cli.command('cache')
@click.option('--port', default=os.environ['PORT'], help='Port to listen to.')
def kue_dashboard(port):
    """Run Redis Commander."""

    shell("cp","node_modules/redis-commander/config/default.json","node_modules/redis-commander/config/local-development.json")

    link = urlparse(os.environ['REDIS_URL'])

    shell("redis-commander",
        "-u",'/cache',
        "-p",port,
        "--redis-host",link.hostname,
        "--redis-port",link.port,
        "--redis-user",link.username,
        "--redis-password",link.password,
        "--redis-db",link.path[1:] or '0',
    "--noload","--nosave")

#*******************************************************************************

@cli.command('queue')
@click.option('--port', default=os.environ['PORT'], help='Port to listen to.')
def kue_dashboard(port):
    """Run Kue.js dashboard."""

    shell("kue-dashboard",
        "-p",port,
        "-r",os.environ['REDIS_URL'],
    "-q","queue")

#*******************************************************************************

@cli.command('sched')
#@click.option('--port', default=os.environ['PORT'], help='Port to listen to.')
#@click.option('--port', default=os.environ['PORT'], help='Port to listen to.')
def kue_scheduler(port):
    """Run Kue.js scheduler."""

    shell("kue-scheduler",
        "-p",port,
        "-r",os.environ['REDIS_URL'],
    "-q","cache")

#*******************************************************************************

@cli.command('topic')
@click.option('--port', default=os.environ['PORT'], help='Port to listen to.')
@click.option('--work', default=2, help='Number of workers.')
def mqtt_db_gui(port,work):
    """Run mqttDB with GUI."""

    shell("node","engin/topic",
        "-n",os.environ['TOPIC_KEY'],
        "-u",os.environ['TOPIC_URL'],
        "-p",port,
    "-w",work)

#*******************************************************************************

@cli.command('vault')
@click.option('--port', default=os.environ['PORT'], help='Port to listen to.')
def webdav_server(port):
    """Run WebDAV server."""

    shell("node","engin/vault.js")

################################################################################

@cli.command('sqldb')
@click.option('--port', default=os.environ['PORT'], help='Port to listen to.')
def post_graphile(port):
    """Run PostGraphile server on PostgreSQL."""

    shell("postgraphile",
        "-p",port,
        "-c",os.environ.get('DATABASE_URL',"postgres://localhost:5432/memex")+"?ssl=1",
    "--watch")

#*******************************************************************************

@cli.command('nosql')
@click.option('--port', default=os.environ['PORT'], help='Port to listen to.')
@click.option('--front', default='cloud.js', help='FrontOffice script to run with.')
#@click.option('--back', default=None, help='BackOffice script to run with.')
def parse_server(port,front):
    """Run Parse Server on : MongoDB / PostgreSQL."""

    if front is not None:
        shell("node","engin/parse.js",front)
    else:
        shell("node","engin/parse.js")

#*******************************************************************************

@cli.command('graph')
@click.option('--port', default=os.environ['PORT'], help='Port to listen to.')
def neo4j_graph_ql(port):
    """Run Neo4j-GraphQL.js adapter."""

    shell("node","engin/neo4j.js")

################################################################################

@cli.command('proxy')
@click.option('--port', default=os.environ['PORT'], help='Port to listen to.')
@click.option('--schema', default="model/proxy.graphql")
def gql_rest_proxy(port,schema):
    """Run REST proxy for GraphQL."""

    shell("graphql-rest-proxy",schema)

#*******************************************************************************

@cli.command('weave')
@click.option('--port', default=os.environ['PORT'], help='Port to listen to.')
def weaver_gql(port):
    """Run GraphQL Weaver."""

    shell("node","engin/weave.js")

#*******************************************************************************

@cli.command('hyper')
@click.option('--port', default=os.environ['PORT'], help='Port to listen to.')
def hyper_graph_ql(port):
    """Run HyperGraphQL over SparQL."""

    shell("java","-jar","build/libs/hypergraphql-1.0.3-exe.jar","--config","/hgql/config/config.json")

################################################################################

@cli.command('knows')
def ldf_server(port):
    """Run Linked-Data-Fragment server."""

    shell("")

#*******************************************************************************

@cli.command('facts')
@click.option('--port', default=os.environ['PORT'], help='Port to listen to.')
def jena_fuseki(port):
    """Run Apache Jena/Fuseki server."""

    shell("")

#*******************************************************************************

@cli.command('think')
@click.option('--port', default=os.environ['PORT'], help='Port to listen to.')
@click.option('--folder', default="think/")
@click.option('--entail', type=click.Choice(['OWL2RL', 'RDFS'], case_sensitive=False))
@click.option('--reason', type=click.Choice(['inc', 'tag'], case_sensitive=False))
def hylar_owl(port,folder,entail,reason):
    """Run HyLAR Reasoner for OWL/RDFS."""

    shell("hylar",
        "--port",port,
        "--graph-directory",folder,
        "--entailment",entail,
        "--reasoning-method",reason,
    "--no-persist")

################################################################################

if __name__ == '__main__':
    cli()

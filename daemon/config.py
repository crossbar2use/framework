import os

base_dir = os.path.dirname(__file__)
root_dir = lambda *x: os.path.join(base_dir, *x)

MONGO_LINK = "mongodb://lion:Iuor2pGuImMXA7T1@ds119662.mlab.com:19662/lion-bartra"

NEO4J_ADDR = "bolt://hobby-ainbflmcfmlmgbkegbgodgcl.dbs.graphenedb.com:24786"
NEO4J_LINK = "https://hobby-ainbflmcfmlmgbkegbgodgcl.dbs.graphenedb.com:24780"
NEO4J_USER = "lion"
NEO4J_PASS = "b.8EJsp4yoWVB7.Iuor2pGuImMXA7T1"

# Create dummy secrey key so we can use sessions
SECRET_KEY = '123456790'

# Create in-memory database
DATABASE_FILE = root_dir('database.sqlite')
SQLALCHEMY_DATABASE_URI = 'sqlite:///' + DATABASE_FILE
SQLALCHEMY_ECHO = True

# Flask-Security config
SECURITY_URL_PREFIX = "/admin"
SECURITY_PASSWORD_HASH = "pbkdf2_sha512"
SECURITY_PASSWORD_SALT = "ATGUOHAELKiubahiughaerGOJAEGj"

# Flask-Security URLs, overridden because they don't put a / at the end
SECURITY_LOGIN_URL = "/login/"
SECURITY_LOGOUT_URL = "/logout/"
SECURITY_REGISTER_URL = "/register/"

SECURITY_POST_LOGIN_VIEW = "/admin/"
SECURITY_POST_LOGOUT_VIEW = "/admin/"
SECURITY_POST_REGISTER_VIEW = "/admin/"

# Flask-Security features
SECURITY_REGISTERABLE = True
SECURITY_SEND_REGISTER_EMAIL = False
SQLALCHEMY_TRACK_MODIFICATIONS = False

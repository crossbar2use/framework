#!/bin/bash

source venv/bin/activate

export `cat .env`

export GHOST_DOMAIN="tayaa"
export GHOST_NARROW="connectik"
export GHOST_PERSON="tayamino"

export GHOST_TARGET="tohoku.herokuapp.com"
export GHOST_REALMS=""

export GHOST_MODULE="tohoku"
export GHOST_PARAMS="xxxxxxxx"

export GHOST_CYPHER=abbb53dfb9f048cfad0989032b909106
export GHOST_SECRET=0ea3327af5a543aa81e97c169523b8a1

git_repo () {
    if [[ ! -d $1 ]] ; then
        if [[ -d $HOME/.ssh/id_rsa ]] ; then
            git clone git@$2:$3.git $1
        else
            git clone https://$GHOST_PERSON:$GHOST_CYPHER"@"$2:$3.git $1
        fi
    fi
}

for key in ronin ninja vault ghost ; do
    case $key in
        ghost)
            git_repo $key "bitbucket.org" IT-Ronin/$GHOST_PERSON
            ;;
        sohei)
            git_repo $key "bitbucket.org" IT-Ronin/framework4shell
            ;;
        ronin)
            git_repo $key "bitbucket.org" IT-Ronin/framework4python
            ;;
        ninja)
            git_repo $key "bitbucket.org" IT-Ronin/framework4nodejs
            ;;
    esac
done

-r ronin/requirements.txt

gevent
PyYAML<=3.12,>=3.11

sentry
sentry-github

dj_database_url
django-bcrypt
django-secure

flask-sentinel
connexion

flask-sslify

airflow[postgres,crypto,password,celery] == 1.7.1.3
rauth

luigi
sciluigi
luigi-monkey-patch
luigi-monitor
luigi-slack

#law
#lex
#exl
#axel2
#waluigi
#luiti

hyperboard

dash==0.21.0
dash-renderer==0.11.3
dash-html-components==0.9.0
dash-core-components==0.18.1
plotly

flask-grapĥql
graphjoiner

click
colorama
eventlet
feedparser
requests
Werkzeug
tqdm

mongoengine
py2neo

Flask
Flask-Admin
Flask-SQLAlchemy
Flask-Security>=1.7.5
